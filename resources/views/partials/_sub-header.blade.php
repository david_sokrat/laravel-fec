<section id="sub-header">
    <div class="wrapper-sub-header">
        <div class="company-directions">
            <p>{{ $translations['subheader_label'] }}</p>
        </div>
        <div class="align-sub-header">
            <div class="directions-items">
                <nav class="sub-header-menu">
                    <ul class="sub-header-ul">
                        <li><a class="{{ Route::currentRouteName() == "finance" ? "active-link" : "" }}" href="{{ route('finance', Session::get('locale')) }}">{{ $translations['subheader_nav_finance'] }}</a></li>
                        <li><a class="{{ Route::currentRouteName() == "audit" ? "active-link" : "" }}" href="{{ route('audit', Session::get('locale')) }}">{{ $translations['subheader_nav_audit'] }}</a></li>
                        <li><a class="{{ Route::currentRouteName() == "legal" ? "active-link" : "" }}" href="{{ route('legal', Session::get('locale')) }}">{{ $translations['subheader_nav_legal'] }}</a></li>
                        <li><a class="{{ Route::currentRouteName() == "academy" ? "active-link" : "" }}" href="{{ route('academy', Session::get('locale')) }}">{{ $translations['subheader_nav_academy'] }}</a></li>
                        <li><a class="{{ Route::currentRouteName() == "tax-dispute" ? "active-link" : "" }}" href="{{ route('tax-dispute', Session::get('locale')) }}">{{ $translations['subheader_nav_tax_dispute'] }}</a></li>
                    </ul>
                </nav>
            </div>
            {{--<div class="languages">
                {!! Form::open(['route' => 'language.change']) !!}
                    <button class="{{ Session::get('locale') == 'ru' ? "not-visible " : "language-select-sub-header lang-btn" }}" type="submit" id="language-select-sub-header" name="lang" value="ru">RU</button>
                    <button class="{{ Session::get('locale') == 'en' ? "not-visible " : "language-select-sub-header lang-btn" }}" type="submit" id="language-select-sub-header1" name="lang" value="en">EN</button>
                    <button class="{{ Session::get('locale') == 'ka' || Session::get('locale') == '' ? "not-visible " : "language-select-sub-header lang-btn" }}" type="submit" id="language-select-sub-header2" name="lang" value="ka">KA</button>
                    <button class="{{ Session::get('locale') == 'tr' ? "not-visible " : "language-select-sub-header lang-btn" }}" type="submit" id="language-select-sub-header3" name="lang" value="tr">TR</button>
                {!! Form::close() !!}
            </div>--}}
        </div>
        <div id="town-of-interest">
            <div class="toggle-town">
                <form id="toggle-town-sub-header" class="toggle-town" action="{{ route('set-contacts', Session::get('locale')) }}" method="POST">
                    {{ csrf_field() }} 
                    
                    @php $cur_city = Session::get('city') @endphp
                    <h4 class="{{ $cur_city == 'batumi' ? "batumi active-town" : "batumi" }}">{{ $translations['subheader_batumi'] }}</h4>
                    <label class="switch-town"> 
                        <input name="cur_city" value="{{ $cur_city }}" type="hidden">
                        <button class="toggle-city-checkbox" type="submit"></button>
                        <span class="town-slider">
                            <span class="{{ $cur_city == 'tbilisi' ? "town-slider-checked move-city-toggler" : "town-slider-checked" }}"></span>
                        </span>
                    </label>
                    <h4 class="{{ $cur_city == 'tbilisi' ? "tbilisi active-town" : "tbilisi" }}">{{ $translations['subheader_tbilisi'] }}</h4>
                </form>
            </div>
        </div>
    </div>
</section>