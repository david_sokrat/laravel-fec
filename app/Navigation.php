<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    protected $table = 'navigation';

    public function news()
    {
        return $this->hasMany('App\Article');
    }
}
