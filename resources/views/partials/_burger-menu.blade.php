<section id="burger-menu">
    <div class="wrapper-burger-menu">
        <div class="logo">
            <a href="/">
                <img src="{{ asset('img/fec_logo.svg') }}" alt="FEC logo" />
            </a>
        </div>
        <div class="client-profile">
            @if(Auth::guest())
                <a class="profile-login-btn" href="#"><img src="{{ asset('img/user-shape.svg') }}" /></a>
            @endif
            @if(Auth::check())
                <a class="profile-logout-btn" href="{{ route('logout', Session::get('locale')) }}"><img src="{{ asset('img/logout.svg') }}" /></a>
            @endif
        </div>
        <div class="burger-icon">
            <div class="burger"></div>
        </div>
    </div>
</section>