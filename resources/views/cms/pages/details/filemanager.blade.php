@extends('backend.layouts.backend')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">


                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">{{tc('details')}}</a></li>
                        <li class="active">{{tc('file.manager')}}</li>
                    </ol>



                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" > <br>

                        <iframe src="/manager/laravel-filemanager" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>





                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

