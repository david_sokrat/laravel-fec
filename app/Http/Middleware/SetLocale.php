<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // session::put('locale', 'ka');

        // if ($request->method() === 'GET')
        // {
        //     $requestLocale = $request->segment(1);

        //     if (array_key_exists($requestLocale, config('translatable.locales')))
        //     {
        //         session(['locale' => $requestLocale]);
        //     }
        //     else if (!session()->has('locale'))
        //     {
        //         $browserLanguage = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        //         if (array_key_exists($browserLanguage, config('translatable.locales')))
        //         {
        //             session(['locale' => $browserLanguage]);
        //         }
        //         else
        //         {
        //             session(['locale' => config('app.fallback_locale')]);
        //         }
        //     }

        //     app()->setLocale(session('locale'));
        // }

        // return $next($request);

        $lang = $request->segment(1);

        if (!empty($lang)) {
            Session::put('locale', $lang);
        } else {
            Session::put('locale', 'ka');
        }

        if (in_array($lang, Config::get('translatable.langs'))) {
            App::setLocale($lang);
        } else {
            if (strlen($lang) === 2) {
                app()->abort(404);
            }
        }

        return $next($request);
    }
}
