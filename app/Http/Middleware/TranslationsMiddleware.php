<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App;
use Session;
use URL;
use View;
use Translation;
use TranslationContent;

class TranslationsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $transCheck = Session::get('locale');

        if ($transCheck == '') {
            $transCheck = 'ka';
        } else {
            $transCheck = Session::get('locale');
        }

        $locs = DB::select('select * from translations where locale = ?', [$transCheck]);
        $locs_content = DB::select('select * from translations_contents where locale = ?', [$transCheck]);

        $translations = array();
        $translations_contents = array();

        foreach ($locs as $loc) {
            $translations[$loc->trans_key] = $loc->value;
        }

        foreach ($locs_content as $loc_content) {
            $translations_contents[$loc_content->trans_title_key] = $loc_content->title_value;
            $translations_contents[$loc_content->trans_body_key] = $loc_content->body_value;
            $translations_contents[$loc_content->trans_slug_key] = $loc_content->slug;
        }

        View::share('translations', $translations);
        View::share('translations_contents', $translations_contents);

        return $next($request);
    }
}
