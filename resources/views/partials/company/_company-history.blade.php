<section id="company-history">
    <div class="wrapper">
        <h1>{{ $translations['company_page_title'] }}</h1>
        <div class="company-history-items">
            <div class="company-history-item main-header">{{ $translations['company_promo_fitst'] }}</div>
            <div class="company-history-item">
                <h2>{{ $translations['company_promo_h2_history'] }}</h2>
                <p>{{ $translations['company_promo_second'] }}</p>
            </div>
            <div class="company-history-item">
                <h2>{{ $translations['company_promo_h2_fin_school'] }}</h2>
                <p>{{ $translations['company_promo_third'] }}</p>
            </div>
            <div class="company-history-item">
                <h2>{{ $translations['company_promo_h2_secure'] }}</h2>
                <p>{{ $translations['company_promo_fourth'] }}</p>
            </div>
        </div>
    </div>
</section>