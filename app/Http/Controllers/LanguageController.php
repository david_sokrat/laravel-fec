<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App;
use Session;
use URL;

use App\Translation;

class LanguageController extends Controller
{
    public function postChangeLanguage(Request $request)
    {
        $changeLang = $request->input('lang');
        $userLang = session('locale');

        if ($userLang !== $changeLang && array_key_exists($changeLang, config('translatable.locales')))
        {   
            session(['locale' => $changeLang]);
            app()->setLocale($changeLang);

            $segments = str_replace(url('/'), '', url()->previous());
            $segments = array_filter(explode('/', $segments));
            array_shift($segments);
            array_unshift($segments, $changeLang);

            return redirect()->to(implode('/', $segments));
        }

        return redirect()->back();
    }

    public function cmsChangeLanguage(Request $request)
    {
        $changeLang = $request->input('lang');
        $userLang = session('locale');

        session(['locale' => $changeLang]);
        app()->setLocale($changeLang);

        return redirect()->back();
    }
}
