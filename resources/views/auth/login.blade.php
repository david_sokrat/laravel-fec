<form id="auth-form" method="POST" action="{{ route('login', Session::get('locale')) }}">
    {{ csrf_field() }}
    
    <h2>{{ $translations['user_login_modal_title'] }}</h2>
    <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="user-email" placeholder="{{ $translations['user_login_name'] }}" name="email" value="{{ old('email') }}" required autofocus>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="input-fields{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="user-password" placeholder="{{ $translations['user_password'] }}" name="password" required>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    {{-- <div class="input-fields">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>{{ trans('auth.remember_check_box') }}
            </label>
        </div>
    </div> --}}

    <div class="auth-btn-wrap">
        <button type="submit" class="login-btn">{{ $translations['user_login_btn'] }}</button>
    </div>

    <div class="auth-password-reestablish">
        <a href="{{ route('password.request', Session::get('locale')) }}"><span id="req-email-modal-link">{{ $translations['user_email_req_btn'] }}<span></a>
    </div>
</form>
            
      
