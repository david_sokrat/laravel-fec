<?php 
    $pdf_href; 
    if (Session::get('locale') === 'ka' || Session::get('locale') === '') {
        $pdf_href = '/files/fec_presentation_ka.pdf';
    } else if (Session::get('locale') === 'en') {
        $pdf_href = '/files/fec_presentation_en.pdf';
    } else if (Session::get('locale') === 'ru') {
        $pdf_href = '/files/fec_presentation_ru.pdf';
    }
?>

<section id="promo-block">
    <div class="wrapper">
        <h1>{{ $translations['promo_block'] }}</h1>
        <p>{{ $translations['promo_content'] }}</p>
        <p class="second-p">{{ $translations['promo_content_second'] }}
            <a class="russia-a" href={{ $pdf_href }} target="blank">{{ $translations['promo_block_presentation'] }}</a>
        </p>
    </div>
</section>