<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" style="display: flex;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        @if('')
                            <img src="{{ asset('storage/public/img/avatar.svg') }}" alt="..." class="img-circle profile_img" style="width: 40px; height: 40px; margin-top: 0;">
                        @else
                            <img src="{{ asset('storage/public/img/avatar.svg') }}" alt="..." class="img-circle profile_img" style="width: 40px; height: 40px; margin-top: 0;">
                        @endif

                        <span style="line-height: 40px; margin-right: 5px; width: 100px;">Name</span>
                        <span class=" fa fa-angle-down" style="line-height: 40px; margin-right: 10px; width: 10px;"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ route('cms.profile') }}">{{ $translations['header_nav_profile'] }}</a></li>
                        <li><a id="open-reg-form"><span>{{ $translations['cms_topnav_register'] }}<span></a></li>
                        <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i>{{ $translations['cms_topnav_logout'] }}</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<section id="register-modal">
    <div class="register-modal-wrapper">
        <div class="auth-icon-modal-cms">
            <div class="auth-modal-close-cms"></div>
        </div>

        <form id="register-form" method="POST" action="{{ route('register', Session::get('locale')) }}">
            {{ csrf_field() }}

            <h2>{{ $translations['cms_topnav_register_form_title'] }}</h2>
            <div class="input-fields{{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="user-name" placeholder="{{ $translations['user_login_name'] }}" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="user-email" placeholder="{{ $translations['audit_form_company_email'] }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-fields{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="user-password" placeholder="{{ $translations['reset_password'] }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-fields">
                <input type="password" class="user-password" placeholder="{{ $translations['confirm_reset_password'] }}" name="password_confirmation" required>
            </div>

            <div class="input-fields">
                <button type="submit" class="btn btn-primary">{{ $translations['cms_topnav_register_form_btn'] }}</button>
            </div>
        </form>
    </div>
</section>
<!-- /top navigation -->