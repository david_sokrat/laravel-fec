{{-- @extends('main')

@section('title', '| Create article')

@section('stylesheets')
    <link href="{{ asset('css/tax-dispute.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    <section id="tax-order-form" class="form-dispute-page">
        <div class="wrapper">
            <h2>Создайте новость</h2>
            {!! Form::open(['route' => ['store-article', Session::get('locale')], 'files' => true, 'id' => 'tax-dispute-form', 'data-parsley-validate' => '', 'method' => 'PUT']) !!}
                {{ Form::label('title', 'Заголовок: ', array()) }}
                {{ Form::text('title', null, array('class' => 'input-fields', 'maxlength' => '250', 'required' => '')) }}
            
                {{ Form::label('date', 'Дата новости: ', array()) }}
                {{ Form::date('date', null, array('class' => 'input-fields', 'required' => '')) }}

                {{ Form::label('slug', 'Читабельный адрес страницы: ', array()) }}
                {{ Form::text('slug', null, array('class' => 'input-fields', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}

                {{ Form::label('navigation_id', 'Год новости: ', array()) }}
                <select class="input-fields" name="navigation_id">
                    @foreach ($navigation as $nav)
                        <option value="{{ $nav->id }}">{{ $nav->year }}</option>
                    @endforeach
                </select>

                {{ Form::label('image_name', 'Загрузить картинку: ', array()) }}
                {{ Form::file('image_name', null, array('class' => 'btn-save', 'required' => '')) }}
                
                {{ Form::label('body', 'Текст новости: ', array()) }}
                {{ Form::textarea('body', null, array('class' => 'input-fields', 'required' => '')) }}

                {{ Form::submit('СОЗДАТЬ НОВОСТЬ', array('class' => 'btn-create')) }}
            {!! Form::close() !!}
        </div>
    </section>
@endsection --}}