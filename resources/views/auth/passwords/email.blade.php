@extends('main')

@section('title', '| Email request')

@section('scripts')
    <script type="text/javascript">
        mainStatus = true;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

<section id="request-email-modal" style="display: block;">
    <div class="request-email-modal-wrapper">
        <a href="{{ route('home') }}">
            <div class="auth-icon-modal active">
                <div class="auth-modal-close"></div>
            </div>
        </a>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form id="request-email" method="POST" action="{{ route('password.email', Session::get('locale')) }}">
            {{ csrf_field() }}

            <h2>{{ $translations['user_email_password_drop_down_title'] }}</h2>
            <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="user-email" placeholder="{{ $translations['audit_form_company_email'] }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="req-btn-wrap">
                <button type="submit" class="req-email-btn"><span>{{ $translations['user_password_drop_down_btn'] }}</span></button>
            </div>
        </form>
    </div>
</section>

@section('content')
    @include('partials.index._promo-block')
    @include('partials.index._brilliant-services')
    @include('partials.index._company-achivments')
    @include('partials.index._company-offers')
    @include('partials.index._order-form')
    @include('partials.index._useful-docs')
    @include('partials.index._partners')
@endsection
