@include('cms.common.header')

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col menu_fixed">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{ route('home') }}" class="site_title"><i class="fa fa-paw"></i> <span>ITERS • CMS</span></a>
                    </div>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <div class="clearfix"></div>
@include('cms.common.sidebar')
@include('cms.common.topnav')

@yield('content')

@include('cms.common.footer')
