/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.filebrowserBrowseUrl = '/back/ckeditor/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserImageBrowseUrl = '/back/ckeditor/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserFlashBrowseUrl = '/back/ckeditor/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '/back/ckeditor/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = '/back/ckeditor/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '/back/ckeditor/kcfinder/upload.php?opener=ckeditor&type=flash';


    // config.removeButtons = 'Save,Print,Preview,NewPage';
    // config.allowedContent = true;

    /*config.filebrowserFlashUploadUrl = '/manager/laravel-filemanager/upload?type=Images&_token=';*/
};

CKEDITOR.on( 'instanceReady', function( evt ) {
    evt.editor.dataProcessor.htmlFilter.addRules( {
        elements: {
            img: function(el) {
                el.addClass('ck_img');
            },
            div: function(el) {
                el.addClass('ck_img_wrapper');
            }
        }
    });
});
