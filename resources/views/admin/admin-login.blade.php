<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('partials._head')
    </head>

    <body id="body">
        <div style="position: fixed !important; top: 20px !important;">
            @include('partials._messages')
        </div>

        <section id="auth-modal-admin" style="display: block;">
            <div class="auth-modal-admin-wrapper">
                <a href="{{ route('home') }}">
                    <div id="close-admin-auth-modal" class="auth-icon-modal active">
                        <div class="auth-modal-close"></div>
                    </div>
                </a>

                <form id="auth-form-admin" method="POST" action="{{ route('admin.login.submit', Session::get('locale')) }}">
                    {{ csrf_field() }}

                    <h2 class="admin-modal-header">{{ $translations['admin_auth_title'] }}</h2>
                    <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="user-email" placeholder="{{ $translations['user_login_name'] }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-fields{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="user-password" placeholder="{{ $translations['user_password'] }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    {{-- <div class="input-fields">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>{{ trans('auth.remember_check_box') }}
                            </label>
                        </div>
                    </div> --}}

                    <div class="auth-btn-wrap">
                        <button type="submit" class="login-btn">{{ $translations['user_login_btn'] }}</button>
                    </div>

                    <div class="auth-password-reestablish">
                        <a class="forgott-pass-link" href="{{ route('admin.password.request') }}"><span id="req-email-modal-link">{{ $translations['user_email_req_btn'] }}<span></a>
                    </div>
                </form>
            </div>
        </section>

        @include('partials._javascript')

    </body>
</html>