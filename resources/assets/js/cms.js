$(document).ready(function() {
    $('#open-reg-form').click(function(e) {
        e.preventDefault();

        $('#register-modal').fadeIn(350);
    });

    $('.auth-icon-modal-cms').click(function(e) {
        e.preventDefault();
        
        $('#register-modal').fadeOut(350);
    });

    $('#delete-report').click(function(e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-report').fadeIn(350);
    });

    $('#cancel-del-report').click(function(e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-report').fadeOut(350);
    });

    $('#delete-article').click(function(e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-article').fadeIn(350);
    });

    $('#cancel-del-article').click(function(e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-article').fadeOut(350);
    });

    $('.language-select-cms').on('change', function () {
        $(this).closest('form').submit();
    });
});

//Chache clear srcipt
$('.cache_clear').click(function () {
    var data = {
        _token : '{{csrf_token()}}',
    };

    $.post(`{{ route('cms.settings.cache.clear') }}`, data)
        .done(function(data) {
            location.reload();
        }
    );
});