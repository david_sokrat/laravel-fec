<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\translationContent;

class Article extends Model
{
    public function translationContent()
    {
        return $this->hasMany('App\TranslationContent');
    }
}
