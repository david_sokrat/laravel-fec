@extends('main')

@section('title', '| Legal')

@section('stylesheets')
    <link href="{{ asset('css/legal.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = true;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('canvas')
    <canvas id="canvas1"></canvas>
@endsection

@section('content')
    @include('partials.legal._tax-dispute-promo') 
    @include('partials.legal._tax-dispute-text-content') 
    @include('partials.legal._tax-order-form') 
@endsection