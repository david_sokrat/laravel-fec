@extends('backend.layouts.backend')


@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">


                    <ol class="breadcrumb bread-primary ">

                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>

                        <li><a href="#">{{tc('details')}}</a></li>

                        <li class="active">{{tc('social.networks')}}</li>


                    </ol>



                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" > <br>


                        <form class="form-horizontal form-label-left" method="post" action="{{route('cms.social.store')}}">

                            {{ csrf_field() }}





                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="facebook" value="{{$social->facebook}}" class="form-control has-feedback-left" id="inputSuccess1" placeholder="Facebook">
                                <span class="fa fa-facebook form-control-feedback left" aria-hidden="true"></span>
                            </div>


                         {{--   <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="hidden" name="vk" value="{{$social->vk}}" class="form-control has-feedback-left" id="inputSuccess2" placeholder="ВКонтакте">
                                <span class="fa fa-vk form-control-feedback left" aria-hidden="true"></span>
                            </div>


                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="hidden" name="twitter" value="{{$social->twitter}}" class="form-control has-feedback-left" id="inputSuccess3" placeholder="Twitter">
                                <span class="fa fa-twitter form-control-feedback left" aria-hidden="true"></span>
                            </div>
--}}

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="instagram" value="{{$social->instagram}}" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Instagram">
                                <span class="fa fa-instagram form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="youtube" value="{{$social->youtube}}" class="form-control has-feedback-left" id="inputSuccess5" placeholder="Youtube">
                                <span class="fa fa-youtube form-control-feedback left" aria-hidden="true"></span>
                            </div>


                         {{--   <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="hidden" name="linkedin" value="{{$social->linkedin}}" class="form-control has-feedback-left" id="inputSuccess6" placeholder="linkedin">
                                <span class="fa fa-linkedin form-control-feedback left" aria-hidden="true"></span>
                            </div>
--}}



                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <button type="submit" class="btn btn-success">{{tc('save')}}</button>
                            </div>





                        </form>












                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

