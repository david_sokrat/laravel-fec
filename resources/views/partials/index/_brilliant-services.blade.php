<section id="brilliant-services">
    <canvas id="canvas"></canvas>
    <script id="template" type="notjs">
        <div class="scene"></div>
    </script>
    <div class="wrapper">
        <div class="brilliants">
            <div class="purple-brilliant brl-item">
                <div class="brl-wrap" id="content0">
                    <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
                </div>
                <div class="prl-text">
                    <a id="brl-text-container" href="{{ route('finance', Session::get('locale')) }}">
                        <h2 id="brl-prl-h2">{{ $translations['finance.main'] }}
                            <svg version="1.1" class="brl-arrow" id="purple-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="22px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
                                <g id="arrow-forward">
                                    <polygon fill="#6f34e0" points="204,0 168.3,35.7 311.1,178.5 0,178.5 0,229.5 311.1,229.5 168.3,372.3 204,408 408,204"/>
                                </g>
                            </svg>
                        </h2>
                    </a>
                </div>
            </div>
            <div class="orange-brilliant brl-item">
                <div class="brl-wrap" id="content1">
                    <!--<img src="{{ asset('img/legal.svg') }}" alt="Legal support and consultation" />-->
                </div>
                <div class="orng-text">
                    <a href="{{ route('legal', Session::get('locale')) }}">
                        <h2 id="brl-orng-h2">{{ $translations['legal.main'] }}
                            <svg version="1.1" class="brl-arrow" id="orange-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="22px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
                                <g id="arrow-forward">
                                    <polygon fill="#d8794a" points="204,0 168.3,35.7 311.1,178.5 0,178.5 0,229.5 311.1,229.5 168.3,372.3 204,408 408,204"/>
                                </g>
                            </svg>
                        </h2>
                    </a>
                </div>
            </div>
            <div class="yellow-brilliant brl-item">
                <div class="brl-wrap" id="content2">
                    <!--<img src="{{ asset('img/academy.svg') }}" alt="Training of future financiers" />-->
                </div>
                <div class="ylw-text">
                    <a href="{{ route('academy', Session::get('locale')) }}">
                        <h2 id="brl-ylw-h2">{{ $translations['academy.main'] }}
                            <svg version="1.1" class="brl-arrow" id="yellow-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="22px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
                                <g id="arrow-forward">
                                    <polygon fill="#e2e249" points="204,0 168.3,35.7 311.1,178.5 0,178.5 0,229.5 311.1,229.5 168.3,372.3 204,408 408,204"/>
                                </g>
                            </svg>
                        </h2>
                    </a>                           
                </div>
            </div>
            <div class="green-brilliant brl-item">
                <div class="brl-wrap" id="content3">
                    <!--<img src="{{ asset('img/audit.svg') }}" alt="Auditing services" />-->
                </div>
                <div class="grn-text">
                    <a href="{{ route('audit', Session::get('locale')) }}">
                        <h2 id="brl-grn-h2">{{ $translations['audit.main'] }}                               
                            <svg version="1.1" class="brl-arrow" id="green-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="22px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
                                <g id="arrow-forward">
                                    <polygon fill="#3b9694" points="204,0 168.3,35.7 311.1,178.5 0,178.5 0,229.5 311.1,229.5 168.3,372.3 204,408 408,204"/>
                                </g>
                            </svg>
                        </h2>
                    </a>                           
                </div>
            </div>
            <div class="red-brilliant brl-item">
                <div class="brl-wrap" id="content4">
                    <!--<img src="{{ asset('img/debtors.svg') }}" alt="Support in matters with debtors" />-->
                </div>
                <div class="rd-text">
                    <a href="{{ route('tax-dispute', Session::get('locale')) }}">
                        <h2 id="brl-rd-h2">{{ $translations['tax_dispute.main'] }}
                            <svg version="1.1" class="brl-arrow" id="red-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="23px" height="22px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve">
                                <g id="arrow-forward">
                                    <polygon fill="#ff1f1f" points="204,0 168.3,35.7 311.1,178.5 0,178.5 0,229.5 311.1,229.5 168.3,372.3 204,408 408,204"/>
                                </g>
                            </svg>
                        </h2>
                    </a>                          
                </div>
            </div>
        </div>
    </div>
</section>