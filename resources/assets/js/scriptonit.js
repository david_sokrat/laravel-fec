if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
var container;
var camera, cameraTarget, scene, renderer;
var group, textMesh1, textMesh2, textGeo, material;
var firstLetter = true;
var text = 'ВЕДЕНИЕ И КОНСУЛЬТАЦИЯ БУХГАЛТЕРИИ',
    height = 1,
    size = 16,
    hover = 50,
    curveSegments = 10,
    bevelThickness = 1,
    bevelSize = 1.5,
    bevelSegments = 13;
var font = null;
var targetRotation = 0;
var targetRotationOnMouseDown = 0;
var mouseX = 0;
var mouseXOnMouseDown = 0;
var windowHalfX = window.innerWidth;
var windowHalfY = window.innerHeight;

init();
animate();

function init() {
    container = document.getElementById( 'container' );
    // CAMERA
    camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 1500 );
    camera.position.set( 0, 280, 700 );
    cameraTarget = new THREE.Vector3( 0, 300, 0 );
    // SCENE
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x100d20);
    // LIGHT
    var light = new THREE.AmbientLight(0xffffff, 1.5);
    scene.add(light);
    
    material = new THREE.MeshPhongMaterial({color: 0xffffff, flatShading: true});
    group = new THREE.Group();
    group.position.y = 200;
    scene.add(group);

    var loader = new THREE.TTFLoader();
    loader.load( '../fonts/MuseoSansCyrl-300.ttf', function (json) {
        font = new THREE.Font(json);
        createText();
    });

    var plane = new THREE.Mesh(
        new THREE.PlaneBufferGeometry( 1000, 1000),
        new THREE.MeshBasicMaterial({color: 0xffffff, opacity: 0, transparent: true })
    );
    plane.position.y = 0;
    plane.rotation.x = 0;
    scene.add(plane);
    // RENDERER
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    // EVENTS
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'touchstart', onDocumentTouchStart, false );
    document.addEventListener( 'touchmove', onDocumentTouchMove, false );
    window.addEventListener( 'resize', onWindowResize, false );
}

function onWindowResize() {
    windowHalfX = window.innerWidth;
    windowHalfY = window.innerHeight;
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function createText() {
    textGeo = new THREE.TextBufferGeometry( text, {
        font: font,
        size: size,
        height: height,
        curveSegments: curveSegments,
        bevelThickness: bevelThickness,
        bevelSize: bevelSize,
        bevelEnabled: true
    });

    textGeo.computeBoundingBox();
    textGeo.computeVertexNormals();

    var centerOffset = - 0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);
    textMesh1 = new THREE.Mesh(textGeo, material);
    textMesh1.position.x = centerOffset;
    textMesh1.position.y = hover;
    textMesh1.position.z = 0;
    textMesh1.rotation.x = 0;
    textMesh1.rotation.y = Math.PI * 2;
    group.add(textMesh1);
}

function refreshText() {
    group.remove(textMesh1);

    if (!text) return;

    createText();
}

function onDocumentMouseDown( event ) {
    event.preventDefault();
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.addEventListener( 'mouseout', onDocumentMouseOut, false );
    mouseXOnMouseDown = event.clientX - windowHalfX;
    targetRotationOnMouseDown = targetRotation;
}
function onDocumentMouseMove( event ) {
    mouseX = event.clientX - windowHalfX;
    targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.02;
}
function onDocumentMouseUp( event ) {
    document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
    document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
    document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentMouseOut( event ) {
    document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
    document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
    document.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onDocumentTouchStart( event ) {
    if ( event.touches.length === 1 ) {
        event.preventDefault();
        mouseXOnMouseDown = event.touches[ 0 ].pageX - windowHalfX;
        targetRotationOnMouseDown = targetRotation;
    }
}
function onDocumentTouchMove( event ) {
    if ( event.touches.length === 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.05;
    }
}

function animate() {
    requestAnimationFrame(animate);
    group.rotation.y += (targetRotation - group.rotation.y) * 0.05;
    camera.lookAt(cameraTarget);
    renderer.render(scene, camera);
}