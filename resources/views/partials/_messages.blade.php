@if (Session::has('success'))
    <div class="success-message">
        <strong class="success-strong">Success:</strong> {{ Session::get('success') }}
    </div>
    {{ Session::forget('success') }}
@endif

@if (Session::has('error'))
    <div class="error-message">
        <strong class="error-strong">Error:</strong> {{ Session::get('error') }}
        {{-- <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul> --}}
    </div>
    {{ Session::forget('error') }}
@endif