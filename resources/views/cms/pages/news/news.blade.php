@extends('cms.layouts.backend')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="{{ route('news.index') }}">{{ $translations['cms_sidebar_nav_all_news'] }}</a></li>
                    </ol>

                    @include('partials._messages')
                    
                    <div class="x_content"> <br>
                        <div class="table-responsive">
                            <a href="{{ route('news.create') }}" class="btn btn-success btn-xs"><i class="material-icons" style="line-height: 3rem;">add_circle</i></a>
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">{{ $translations['cms_item_id'] }}</th>
                                        <th class="column-title">{{ $translations['cms_item_slug'] }}</th>
                                        <th class="column-title">{{ $translations['cms_item_title'] }}</th>
                                        <th class="column-title">{{ $translations['cms_item_date'] }}</th>
                                        <th class="column-title no-link last" style="text-align: end;"><span class="nobr" style="margin-right: 15px;">{{ $translations['cms_item_action'] }}</span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if(!$news->isEmpty())

                                        @foreach($news as $new)
                                            <tr class="even pointer">
                                                <td class=" " style="vertical-align: middle; !important;line-height: 1rem;">{{ $new->id }}</td>
                                                <td class=" " style="vertical-align: middle; !important;line-height: 1rem;">{{ $translations_contents[$new->slug] }}</td>
                                                <td class=" " style="vertical-align: middle; !important;line-height: 1rem;">{{ $translations_contents[$new->title] }}</td>
                                                <td class=" " style="line-height: 1rem; vertical-align: middle !important;">{{ $new->date }}</td>
                                                <td class=" " style="width: 130px; padding-top: 20px !important;line-height: 1rem; text-align: end;">
                                                    {!! Form::open(['route' => ['news.edit', $new->id], 'style' => 'display: inline-block', 'method' => 'GET']) !!} 
                                                        {!! Form::submit('mode_edit', ['class' => 'material-icons btn btn-default', 'title' => 'Edit']) !!}  
                                                    {!! Form::close() !!} 
                                                    <form action="{{ route('news.destroy', $new->id) }}" style="display: inline-block" method="DELETE">
                                                        {{ csrf_field() }}
                                                        <button type="submit" name="delete" class="material-icons btn btn-default" style="color: #d30004;" title="Delete">delete</button>
                                                        {{-- <span id="cancel-del-article" class="btn btn-default material-icons" style="cursor: pointer; color: #6b9973; display: inline-block;" title="Cancel">
                                                            cancel
                                                        </span> --}}
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach

                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="paginator col-md-7 col-md-offset-5">{!! $news->links(); !!}</div>
                </div>
            </div>
        </div>
    </div>

    {{-- @if(!$news->isEmpty())
        <!-- remove user modal -->
        <div class="modal-delete-article" 
            style="
            display: none; 
            position: fixed;
            text-align: center; 
            top: 15%;
            left: 50%;
            width: 200px; 
            height: 200px; 
            background: 
            black; 
            z-index: 1000;
            background: rgba(211, 0, 4, .5);
            border: 2px solid rgb(211, 0, 4);
            border-radius: 5px;
            color: white;">
            <div class="modal-wrapper">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4 style="color: white">Are you sure?</h4>
                    <form action="{{ route('news.destroy', $new->id) }}" style="display: inline-block" method="DELETE">
                        {{ csrf_field() }}
                        <button type="submit" name="delete" class="material-icons btn btn-default" style="color: #d30004;" title="Delete">delete</button>
                        <span id="cancel-del-article" class="btn btn-default material-icons" style="cursor: pointer; color: #6b9973; display: inline-block;" title="Cancel">
                            cancel
                        </span>
                    </form>
                </div>
            </div>
        </div>
    @endif --}}

@stop