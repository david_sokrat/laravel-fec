@extends('main')

@section('title', '| Homepage')

@section('scripts')
    <script type="text/javascript">
        mainStatus = true;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    @include('partials.index._promo-block')
    @include('partials.index._brilliant-services')
    @include('partials.index._company-achivments')
    @include('partials.index._company-offers')
    @include('partials.index._order-form')
    @include('partials.index._toggle-news-currency')
    @include('partials.index._useful-docs')
    @include('partials.index._partners')
@endsection