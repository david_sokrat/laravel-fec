<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Locales;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $locales = Locales::all();

        return view('cms/pages/mainpage/mainpage')->withLocales($locales)->withUsers($users);
    }
}