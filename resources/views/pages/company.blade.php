@extends('main')

@section('title', '| Company')

@section('stylesheets')
    <link href="{{ asset('css/company.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = true;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    <div class="wrapper-web-gl">
        <canvas id="canvas"></canvas>
        <script id="template" type="notjs">
            <div class="scene"></div>
        </script>
        <div class="brl-wrap-company0" id="content0"></div>
        <div class="brl-wrap-company1" id="content1"></div>
        <div class="brl-wrap-company2" id="content2"></div>
        <div class="brl-wrap-company3" id="content3"></div>
        <div class="brl-wrap-company4" id="content4"></div>

        @include('partials.company._company-history') 
        @include('partials.index._company-achivments')
        @include('partials.company._employee-galery') 
    </div>
@endsection