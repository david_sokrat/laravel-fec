<section id="tax-dispute-text-content">
    <div class="wrapper">
        <p>{{ $translations['finance.content1'] }}</p>
        <div class="text-content-items">
            <div class="tax-dispute-quota text-content-item">
                {{-- <img id="quotes" src="{{ asset('img/quotes.svg') }}" /> --}}
                <div id="animated_quotes"></div>
                <p>{!! $translations['finance.content2'] !!}</p>
            </div>
             <div class="disputes-descr text-content-item">
                <h2>{{ $translations['finance.content_header'] }}</h2>
                <p>{{ $translations['finance.content3'] }}</p>
<!--                 <p>{{ $translations['finance.content4'] }}</p>
                <p>{{ $translations['finance.content5'] }}</a></p> -->
            </div>
            <div class="disputes-tax-objects text-content-item">
                <h2>{{ $translations['finance.content_header2'] }}</h2>
                <!-- <p>{{ $translations['finance.content6'] }}</p> -->
                <ul>
                    <li>{{ $translations['finance.content_li1'] }}</li>
                    <li>{{ $translations['finance.content_li2'] }}</li>
                    <li>{{ $translations['finance.content_li3'] }}</li>
<!--                     <li>{{ $translations['finance.content_li4'] }}</li>
                    <li>{{ $translations['finance.content_li5'] }}</li>
                    <li>{{ $translations['finance.content_li6'] }}</li> -->
                </ul>
                <!-- <p>{{ $translations['legal.content7'] }}</p> -->
            </div>
            <div class="calculation-tax-disputes text-content-item">
                <h2>{{ $translations['finance.content_header3'] }}</h2>
                <p>{!! $translations['finance.content8'] !!}</p>
                <!--<div class="dispute-table-wrap">
                    <div class="dispute-table">
                        <div class="header-layer"></div>
                        <div class="dispute-table-item">
                            <div class="dispute-debitor dispute-header1">{{ $translations['finance.content_table_deb'] }}</div>
                            <div class="dispute-debitor dispute-data1">60</div>
                            <div class="dispute-debitor dispute-data1">91</div>
                            <div class="dispute-debitor dispute-data1">62</div>
                            <div class="dispute-debitor dispute-data1">55</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-creditor dispute-header2">{{ $translations['finance.content_table_creditor'] }}</div>
                            <div class="dispute-creditor dispute-data2">90</div>
                            <div class="dispute-creditor dispute-data2">68</div>
                            <div class="dispute-creditor dispute-data2">91</div>
                            <div class="dispute-creditor dispute-data2">44</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-amount dispute-header3">{{ $translations['finance.content_table_amount'] }}</div>
                            <div class="dispute-amount dispute-data3">230000</div>
                            <div class="dispute-amount dispute-data3">197000</div>
                            <div class="dispute-amount dispute-data3">8500</div>
                            <div class="dispute-amount dispute-data3">670</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-type-of-record dispute-header4">{{ $translations['finance.content_table_record'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['finance.content_table_profit'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['finance.content_table_deb_amount'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['finance.content_table_deb_refund'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['finance.content_table_new_creditor'] }}</div>
                        </div>
                    </div>
                    <div class="adaptive-header adaptive-header-debit">{{ $translations['finance.content_table_deb'] }}</div>
                    <div class="adaptive-header adaptive-header-credit">{{ $translations['finance.content_table_creditor'] }}</div>
                    <div class="adaptive-header adaptive-header-amount">{{ $translations['finance.content_table_amount'] }}</div>
                    <div class="adaptive-header adaptive-header-record">{{ $translations['finance.content_table_record'] }}</div>
                </div>
            </div> -->
        </div>
    </div>
</section>
<script src="{{ asset('/js/three.min.js') }}"></script>
<script>
    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(75, 200 / 200, 0.5, 100);

    var renderer = new THREE.WebGLRenderer({alpha: true});
    renderer.setSize(200, 200);
    var container = document.getElementById('animated_quotes');
    container.appendChild(renderer.domElement);

    var geometry = new THREE.DodecahedronGeometry(0, 0);
    var material = new THREE.MeshPhongMaterial({wireframe: true, color: 0x6F34E0});
    var cube = new THREE.Mesh(geometry, material);
    scene.add(cube);

    var light = new THREE.DirectionalLight(0xffffff, 1);
    light.position.set(1, 1, 1);
    scene.add(light);

    camera.position.z = 1.8;

    var update = function() {
        
    };

    var render = function() {
        cube.rotation.x += 0.005;
        cube.rotation.y += 0.005;

        renderer.render(scene, camera);
    };

    var animate = function() {
        render();
        requestAnimationFrame(animate);
    };

    animate();
</script>