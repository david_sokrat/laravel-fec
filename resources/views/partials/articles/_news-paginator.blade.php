<section id="news-paginator">
    <div class="news-wrapper">
        <div class="pagination">
            {!! $news->links(); !!}
        </div>
    </div>
</section>