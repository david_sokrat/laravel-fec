<section id="period-reports-table">
    <div class="profile-wrapper">
        <div id="adaptive-period-table-header" data-status="-10" data-date-of-download="-10" data-downloads="-10" class="row table-item row-index1">
            <div id="adaptive-status-arrow-parent" class="row-item1">статус <img id="adaptive-status-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
            <div class="row-item2">название <img id="adaptive-name-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
            <div id="adaptive-load-arrow-parent" class="row-item3">загружено <img id="adaptive-load-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
            <div id="adaptive-amount-arrow-parent" class="row-item4">количество загрузок <img id="adaptive-amount-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
            <div class="row-item5">скачать <img id="adaptive-download-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
        </div>
        <div class="row-items">
            <div data-status="-10" data-date-of-download="-10" data-downloads="-10" class="row table-item reports-table-header row-index1">
                <div id="status-arrow-parent" class="row-item1">статус <img id="status-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
                <div class="row-item2">название <img id="name-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
                <div id="load-arrow-parent" class="row-item3">загружено <img id="load-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
                <div id="amount-arrow-parent" class="row-item4">количество загрузок <img id="amount-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
                <div class="row-item5">скачать <img id="download-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}"></div>
            </div>
            <div data-status="0" data-date-of-download="0" data-downloads="5" class="row table-item row-index2">
                @if($report->status == false) 
                    <div class="row-item1">
                        <svg version="1.1" class="in-progress" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                            <g>
                                <circle fill-rule="evenodd" clip-rule="evenodd" fill="#FFDE00" cx="5" cy="5" r="5"/>
                            </g>
                        </svg>
                    </div>
                @endif

                @if($report->status == true)
                    <div class="row-item1">
                        <svg version="1.1" class="complete" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                            <g>
                                <circle fill-rule="evenodd" clip-rule="evenodd" fill="#20D11C" cx="5" cy="5" r="5"/>
                            </g>
                        </svg>
                    </div>
                @endif
                <div class="row-item2">{{ $report->title }}</div>
                <div class="row-item3">{{ $report->date }}</div>
                <div class="row-item4">5</div>
                <div class="row-item5">
                    <div class="table-docs-download-icons" id="active-docs1">
                        <div class="download-file">
                            <a download href="{{ $report->path_to_excel }}">
                                <img class="docs-for-download" src="{{ asset('img/excel.svg') }}">
                            </a>
                        </div>
                        <div class="download-file">
                            <a download href="{{ $report->path_to_docs }}">
                                <img class="docs-for-download" src="{{ asset('img/docs.svg') }}">
                            </a>
                        </div>
                        <div class="download-file">
                            <a download href="{{ $report->path_to_pdf }}">
                                <img class="docs-for-download" src="{{ asset('img/pdf.svg') }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="back-to-news-link">
            <a href="{{ route('reports', Session::get('locale')) }}"><img src="{{ asset('img/back-to-news-arrow.svg') }}" alt="Back to all news" /> Вернуться ко всем записям</a>
        </div> 
    </div>
</section>