<<table>
    <tr>
        <td>Form name: </td>
        <td>{{ $form_name }}</td>
    </tr>
    <tr>
        <td>Company name: </td>
        <td>{{ $company }}</td>
    </tr>
    <tr>
        <td>Company phone: </td>
        <td>{{ $phone }}</td>
    </tr>
    <tr>
        <td>Maintanance: </td>
        <td>{{ $type_of_maintanance }}</td>
    </tr>
    <tr>
        <td>Address: </td>
        <td>{{ $address }}</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>{{ $email }}</td>
    </tr>
    <tr>
        <td>PC programm: </td>
        <td>{{ $pc_programm }}</td>
    </tr>
    <tr>
        <td>Client's message: </td>
        <td>{{ $client_message }}</td>
    </tr>
</table>