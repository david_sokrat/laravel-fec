<section id="tax-dispute-promo">
    <script id="template3" type="notjs">
        <div class="scene3"></div>
    </script>
    <div id="content3"></div>
    <div class="wrapper">
        <div class="red-brilliant brl-item">
            <div class="brl-wrap">
                <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
            </div>
            <div class="rd-text-text-dispute">
                <h1>{{ $translations['audit.main'] }}</h1>
                <p>{{ $translations['audit.main_text'] }}</p>
            </div>
        </div>
    </div>
</section>