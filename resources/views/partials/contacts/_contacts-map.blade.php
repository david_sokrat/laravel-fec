<section id="contacts-map">
    <div class="wrapper-map">
        <div id="map"></div>
        <div class="shadow-top"></div>
        <div class="shadow-right"></div>
        <div class="shadow-bottom"></div>
        <div class="shadow-left"></div>
    </div>
</section>