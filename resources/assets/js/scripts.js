// WebGl + Threejs animation
function activatePageWebGl(context) {
    if (!Detector.webgl) Detector.addGetWebGLMessage();

    var canvas;
    var scenes = [], renderer;
    
    function init() {
        if (context === true || context === 69) {
            canvas = document.getElementById("canvas");
        } else {
            canvas = document.getElementById("canvas" + context);
        }

        var geometries = [
            new THREE.DodecahedronGeometry(0, 0),
            new THREE.OctahedronBufferGeometry(0, 1),
            new THREE.IcosahedronBufferGeometry(0, 0),
            new THREE.IcosahedronBufferGeometry(0, 0), 
            new THREE.OctahedronBufferGeometry(0, 2)
        ];

        var materials = [
            new THREE.MeshPhongMaterial({wireframe: true, color: 0x6F34E0}),
            new THREE.MeshPhongMaterial({wireframe: true, color: 0xD8794A}),
            new THREE.MeshPhongMaterial({wireframe: true, color: 0xE2E249}),
            new THREE.MeshPhongMaterial({wireframe: true, color: 0x3b9694}),
            new THREE.MeshPhongMaterial({wireframe: true, color: 0xff1f1f})
        ];

        if (context === true || context === 69) {
            var template = document.getElementById("template").text;
        } else {
            var template = document.getElementById("template" + context).text;
        }
        
        if (context === true || context === 69) {
            if (context === true) {
                //38 line is only to activate the default tab on the main page
                //and it is not related to WebGL
                document.getElementById("default-open").click();
            }

            for (var i = 0; i < 5; i++) {
                var scene = new THREE.Scene();
                var content = document.getElementById("content" + i);
                
                var element = document.createElement("div");
                element.className = "list-item";
                element.innerHTML = template.replace('$', i + 1);
    
                scene.userData.element = element.querySelector(".scene");
                content.appendChild(element);
                
                var camera = new THREE.PerspectiveCamera(50, 1, 1, 10);
                camera.position.set(0, 0, 2.5);
                scene.userData.camera = camera;
    
                var controls = new THREE.OrbitControls( scene.userData.camera, scene.userData.element );
                controls.minDistance = 2;
                controls.maxDistance = 5;
                controls.enablePan = false;
                controls.enableZoom = false;
                scene.userData.controls = controls;
                
                var geometry = geometries[i];
                var material = materials[i];
                
                scene.add(new THREE.Mesh(geometry, material));
    
                scene.add(new THREE.HemisphereLight(0xaaaaaa, 0x444444));
    
                var light = new THREE.DirectionalLight(0xffffff, 0.5);
                light.position.set(1, 1, 1);
                scene.add(light);
                scenes.push(scene);
            }

        } else {

            var scene = new THREE.Scene();
            var content = document.getElementById("content" + context);
            
            var element = document.createElement("div");
            element.className = "list-item";
            element.innerHTML = template.replace('$', 1);

            scene.userData.element = element.querySelector(".scene" + context);
            content.appendChild(element);
            
            var camera = new THREE.PerspectiveCamera(50, 1, 1, 10);
            camera.position.set(0, 0, 2.5);
            scene.userData.camera = camera;

            var controls = new THREE.OrbitControls( scene.userData.camera, scene.userData.element );
            controls.minDistance = 2;
            controls.maxDistance = 5;
            controls.enablePan = false;
            controls.enableZoom = false;
            scene.userData.controls = controls;
            
            var geometry = geometries[context];
            var material = materials[context];
            
            scene.add(new THREE.Mesh(geometry, material));

            scene.add(new THREE.HemisphereLight(0xaaaaaa, 0x444444));

            var light = new THREE.DirectionalLight(0xffffff, 0.5);
            light.position.set(1, 1, 1);
            scene.add(light);
            scenes.push(scene);
        }

        renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true, alpha: true});
        renderer.setPixelRatio(window.devicePixelRatio);
    }

    function updateSize() {
        var width = canvas.clientWidth;
        var height = canvas.clientHeight;
        if (canvas.width !== width || canvas.height !== height) {
            renderer.setSize(width, height, false);
        }
    }

    function animate() {
        render();
        requestAnimationFrame(animate);
    }

    function render() {
        updateSize();
        
        renderer.setScissorTest(false);
        renderer.setScissorTest(true);
        scenes.forEach(function(scene) {
            scene.children[0].rotation.y += Math.PI / 500;
            scene.children[0].rotation.x += Math.PI / 1000;

            var element = scene.userData.element;
            var rect = element.getBoundingClientRect();
            
            if (rect.bottom < 0 || rect.top  > renderer.domElement.clientHeight || rect.right  < 0 || rect.left > renderer.domElement.clientWidth) {
                
                return;  
            }
            
            var width  = rect.right - rect.left;
            var height = rect.bottom - rect.top;
            var left   = rect.left;
            var top    = rect.top;
            renderer.setViewport( left, top, width, height );
            renderer.setScissor( left, top, width, height );
            var camera = scene.userData.camera;

            renderer.render(scene, camera);
        });
    }
 
    init();
    animate();
}

function activateTextWebGl(context) {
    var container;
    var camera, scene, renderer, material;
    var text = 'Ведение и консультация бухгалтерии';
    var height = 1,
        size = 10,
        hover = 50,
        curveSegments = 10,
        bevelThickness = 1,
        bevelSize = 1.5,
        bevelSegments = 13;
    var font = null;

    if (context === true) {
        init();
        animate();
    }

    function init() {
        container = document.getElementById('purple-text-container');
        var width = window.innerWidth;
        var height = window.innerHeight;
        
        camera = new THREE.PerspectiveCamera(30,  width / height, 1, 1500);
        camera.position.set(0, 0, 350);
        
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0x100d20);
        
        var light = new THREE.AmbientLight(0xffffff, 1.5);
        scene.add(light);
        
        material = new THREE.MeshPhongMaterial({color: 0xffffff, flatShading: true});

        var loader = new THREE.TTFLoader();
        loader.load( '../fonts/MuseoSansCyrl-300.ttf', function (json) {
            font = new THREE.Font(json);
            createText();
        });

        var plane = new THREE.Mesh(geometry, material);
        var geometry = new THREE.PlaneBufferGeometry(1000, 1000);
        plane.position.y = 0;
        plane.rotation.x = 0;
        scene.add(plane);
        
        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize(300, 300);
        container.appendChild(renderer.domElement);
    }

    function createText() {
        var textGeo = new THREE.TextBufferGeometry(text, {
            font: font,
            size: size,
            height: height,
            curveSegments: curveSegments,
            bevelThickness: bevelThickness,
            bevelSize: bevelSize,
            bevelEnabled: true
        });

        var textMesh1 = new THREE.Mesh(textGeo, material);
        textMesh1.rotation.y += 0.1;
        scene.add(textMesh1);
    }

    function animate() {
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
    }
}

function financePageWebGl(checkStatus) {
    var context = 0;

    if (checkStatus) {
        activatePageWebGl(context);
    }

    return;
}

function legalPageWebGl(checkStatus) {
    var context = 1;

    if (checkStatus) {
        activatePageWebGl(context);
    }

    return;
}

function academyPageWebGl(checkStatus) {
    var context = 2;

    if (checkStatus) {
        activatePageWebGl(context);
    }

    return;
}

function auditPageWebGl(checkStatus) {
    var context = 3;

    if (checkStatus) {
        activatePageWebGl(context);
    }

    return;
}

function taxDisputePageWebGl(checkStatus) {
    var context = 4;

    if (checkStatus) {
        activatePageWebGl(context);
    }

    return;
}

function mainPageWebGl(checkStatus) {

    if (checkStatus) {
        activatePageWebGl(true);
    }

    return;
}

function companyPageWebGl(checkStatus) {

    if (checkStatus) {
        activatePageWebGl(69);
    }

    return;
}

// function textMainPageWebGl(checkStatus) {

//     if (checkStatus) {
//         activateTextWebGl(true);
//     }

//     return;
// }

mainPageWebGl(mainStatus);
financePageWebGl(financeStatus);
legalPageWebGl(legalStatus);
academyPageWebGl(academyStatus);
auditPageWebGl(auditStatus);
companyPageWebGl(companyPageStatus);
taxDisputePageWebGl(taxDisputeStatus);
// contactsPageMap(googleMapStatus);
// textMainPageWebGl(textGlStatus);

//Toggling active main page link 
$('.clicked-main-page-link, .clicked-main-page-link-footer').click(function() {
    $('.burger-icon').removeClass('active');
    $('.clicked-main-page-link').toggleClass('active-link');
    $('.clicked-main-page-link-footer').toggleClass('active-link-footer');
    $('#burger-menu-modal').fadeOut(350);
    $('html, body').animate({scrollTop: $('#brilliant-services').offset().top}, 700);
});

//Toggling tabs with forms and burger menu + burger menu modal window
$(document).ready(function() {
    //Set auto margin for company gallery items
    (function() {
        var self = $('.employee-galery-item');
        
        for (var i = 1; i < self.length; i++) {
            var num;

            if ($(window).width() > 1520) {
                if (i === 1) {
                    num = i;
                } else {
                    num = num + 3;
                }
            }

            if ($(window).width() < 1520) {
                if (i === 2) {
                    num = i;
                } else {
                    num = num + 2;
                }
            }

            if (self[num] !== undefined) {
                self[num].style.marginTop = "50px";
            }
        }
    })();

    $('.language-select-sub-header, .language-select-burger, .language-select-footer').on('change', function () {
        $(this).closest('form').submit();
    });

    $('.toggle-city-checkbox').click(function () {
        $(this).closest('form').submit();
    });

    $('.burger-icon').click(function() {
        $('.burger-icon').toggleClass('active');
        $('.burger-icon-modal').toggleClass('active');
        $('#burger-menu-modal').fadeIn(350);
    });

    $('.slider').click(function() { 
        $('.news-toggle').toggleClass('active-slider');
        $('.currency-toggle').toggleClass('active-slider');
        
        if($('.news-toggle').hasClass('active-slider')) {
            $('.news-main-wrapper').fadeIn(350);
            $('.currency-main-wrapper').fadeOut(350);
        }

        if($('.currency-toggle').hasClass('active-slider')) {
            $('.news-main-wrapper').fadeOut(350);
            $('.currency-main-wrapper').fadeIn(350);
        }
    });

    //Burger menu modal window
    $('.burger-icon-modal').click(function() {
        $('.burger-icon-modal').toggleClass('active');
        $('#burger-menu-modal').fadeOut(350);
        $('.burger-icon').toggleClass('active');
    });

    //Animating arrows in the link on the main page
    $('#brl-prl-h2').mouseover(function() {
        $('#purple-arrow').addClass('move-arrow');
    });

    $('#brl-orng-h2').mouseover(function() {
        $('#orange-arrow').addClass('move-arrow');
    });

    $('#brl-ylw-h2').mouseover(function() {
        $('#yellow-arrow').addClass('move-arrow');
    });

    $('#brl-grn-h2').mouseover(function() {
        $('#green-arrow').addClass('move-arrow');
    });

    $('#brl-rd-h2').mouseover(function() {
        $('#red-arrow').addClass('move-arrow');
    });

    $('#brl-prl-h2').mouseout(function() {
        $('#purple-arrow').removeClass('move-arrow');
    });

    $('#brl-orng-h2').mouseout(function() {
        $('#orange-arrow').removeClass('move-arrow');
    });

    $('#brl-ylw-h2').mouseout(function() {
        $('#yellow-arrow').removeClass('move-arrow');
    });

    $('#brl-grn-h2').mouseout(function() {
        $('#green-arrow').removeClass('move-arrow');
    });

    $('#brl-rd-h2').mouseout(function() {
        $('#red-arrow').removeClass('move-arrow');
    });
});

function openNewTab(evt, formType) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active-form-tab", "");
    }

    document.getElementById(formType).style.display = "block";
    evt.currentTarget.className += " active-form-tab";
}

//Bussiness guide filtering table logic
var binaryDataArray = [];
var categoryDataArray = [];

$("#revenue, #assets, #average, #business").change(function() {
    var selectedOption = "";
    $("#business option:selected").each(function() {
        
        selectedOption = $(this).text();
        
        if (selectedOption) {
            var categoryData = ($(this).data("binary"));
        }
        
        categoryDataArray.splice(0, 1, categoryData);

        return categoryDataArray;
    });

    $("#revenue option:selected").each(function() {
        
        selectedOption = $(this).text();
        
        if (selectedOption) {
            var binaryData = ($(this).data("binary"));
        }
        
        binaryDataArray.splice(0, 1, binaryData);

        return binaryDataArray;
    });

    $("#assets option:selected").each(function() {
        
        selectedOption = $(this).text();
        
        if (selectedOption) {
            var binaryData = ($(this).data("binary"));
        }
        
        binaryDataArray.splice(1, 1, binaryData);

        return binaryDataArray;
    });

    $("#average option:selected").each(function() {
        
        selectedOption = $(this).text();
        
        if (selectedOption) {
            var binaryData = ($(this).data("binary"));
        }
        
        binaryDataArray.splice(2, 1, binaryData);

        return binaryDataArray;
    });
    
    if (binaryDataArray[0] && binaryDataArray[1] && binaryDataArray[2] != false) {
        var binaryKey = parseInt(binaryDataArray.join(''), 2);
    }

    var FIRST_CATEGORY = [2065, 1041, 529, 385, 321, 289, 280, 276, 274, 273];
    var SECOND_CATEGORY = [2082, 2081, 2066, 1058, 1057, 1042, 642, 641, 578, 577, 552, 548, 546, 545, 536, 532, 530, 386, 322, 296, 292, 290];
    var THIRD_CATEGORY = [2116, 2114, 2113, 2084, 2068, 1156, 1154, 1153, 1096, 1092, 1090, 1089, 1064, 1060, 1048, 1044, 644, 584, 580, 388, 328, 324];
    var FOURTH_CATEGORY = [2184, 2180, 2178, 2177, 2120, 2088, 2072, 1160, 648, 392];
    var PIE = 3333;
    var NGO = 3332;
    var OTHERS = 3331;
    var resultZero = {category: '#category-0', select: true};
    var resultFirst = {category: '#category-1', select: false};
    var resultSecond = {category: '#category-2', select: false};
    var resultThird = {category: '#category-3', select: false};
    var resultFourth = {category: '#category-4', select: false};
    var resultFifth = {category: '#category-5', select: true};

    switch (categoryDataArray[0]) {
        case false:
            $('.result-table').addClass('default-table-status');
            $('#revenue, #assets, #average').attr('disabled', true);
            $('#revenue, #assets, #average').css('cursor', 'no-drop');

            for (var i = 0; i < 6; i++) {
                $('#category-' + i).removeClass('matched-category');
            }
            break;
        case NGO:
            $('#revenue, #assets, #average').css('cursor', 'no-drop');
            setResultTableData(resultFifth);
            break;
        case OTHERS:
            $('#revenue, #assets, #average').css('cursor', 'no-drop');
            setResultTableData(resultZero);
            break;
        case PIE:
            $('.result-table').addClass('default-table-status');
            $('#revenue, #assets, #average').attr('disabled', false);
            $('#revenue, #assets, #average').css('cursor', 'pointer');

            for (var i = 0; i < 6; i++) {
                $('#category-' + i).removeClass('matched-category');
            }

            if (FIRST_CATEGORY.includes(binaryKey)) {
                setResultTableData(resultFirst);
            }

            if (SECOND_CATEGORY.includes(binaryKey)) {
                setResultTableData(resultSecond);
            }

            if (THIRD_CATEGORY.includes(binaryKey)) {
                setResultTableData(resultThird);
            }

            if (FOURTH_CATEGORY.includes(binaryKey)) {
                setResultTableData(resultFourth);
            }
            break;
        default:
            setResultTableData(resultZero);
    }

    function setResultTableData(config) {
        for (var i = 0; i < 6; i++) {
            $('#category-' + i).removeClass('matched-category');
        }

        $(config.category).addClass('matched-category');

        if (1 <= i <= 5) {
            $('.result-table').removeClass('default-table-status');
        } else {
            $('.result-table').addClass('default-table-status');
        }

        if(categoryDataArray[0] != PIE) {
            $('#revenue, #assets, #average').val('Choose');
        }
        
        $('#revenue, #assets, #average').attr('disabled', config.select);
    }
}).trigger("change");

//Auth window animation
$('.client-profile').click(function() {
    $('#auth-modal').fadeIn(350);
});

$('.auth-icon-modal').click(function() {
    $('#auth-modal').fadeOut(350);
    $('#request-email-modal').fadeOut(350);
    $('#register-modal').fadeOut(350);
});

$('#req-email-modal-link').click(function() {
    $('#request-email-modal').fadeIn(350);
    $('#auth-modal').fadeOut(350);
    $('#register-modal').fadeOut(350);
});

$('#register-modal-link').click(function() {
    $('#request-email-modal').fadeOut(350);
    $('#auth-modal').fadeOut(350);
    $('#register-modal').fadeIn(350);
});

//News filtering logic 
$('#year2018').click(function(event) {
    filterNews('.year2018');
});

$('#year2017').click(function(event) {
    filterNews('.year2017');
    owlNewsPage.trigger('refresh.owl.carousel');
});

$('#year2016').click(function(event) {
    filterNews('.year2016');
});

function filterNews(context) {
    if (context == '.year2016') {
        $('#year2016').addClass('active-link');
        $('#year2017, #year2018').removeClass('active-link');
        $(context).removeClass('not-visible');
        $('.year2017').addClass('not-visible');
        $('.year2018').addClass('not-visible');
    }

    if (context == '.year2017') {
        $('#year2017').addClass('active-link');
        $('#year2016, #year2018').removeClass('active-link');
        $(context).removeClass('not-visible');
        $('.year2016').addClass('not-visible');
        $('.year2018').addClass('not-visible');
    }

    if (context == '.year2018') {
        $('#year2018').addClass('active-link');
        $('#year2017, #year2016').removeClass('active-link');
        $(context).removeClass('not-visible');
        $('.year2017').addClass('not-visible');
        $('.year2016').addClass('not-visible');
    }
}

//Profile record table filtering logic
//Detecting file downloading icons
$('.table-item').mouseover(function() {
    if ($(this).find('img.docs-for-download') !== '') {
        $(this).find('img.docs-for-download').attr('style', 'opacity: 1');
    }

    $(this).addClass('major-z-index');
});

$('.table-item').mouseout(function() {
    if ($(this).find('img.docs-for-download') !== '') {
        $(this).find('img.docs-for-download').removeAttr('style');
    }

    $(this).removeClass('major-z-index');
});

//Sending id and attributes for sorting to function
$('#status-arrow-parent, #adaptive-status-arrow-parent').click(function() {
    $('#status-arrow, #adaptive-status-arrow').toggleClass('rotate-arrow');
    
    var $statusCheck = $('.row').find('.in-progress');
    
    if ($('#status-arrow, #adaptive-status-arrow').hasClass('rotate-arrow')) {
        if ($statusCheck !== 0) {
            $statusCheck.parent().parent().attr('style', 'display: none');
        } 
    } else {
        $statusCheck.parent().parent().fadeIn(350);
    }
});

$('#load-arrow-parent').click(function() {
    $('#load-arrow').toggleClass('rotate-arrow');

    function delaySorting() {
        sortRecordTableData('data-date-of-download', '#load-arrow');
    }

    setTimeout(delaySorting, 350);
});

$('#adaptive-load-arrow-parent').click(function() {
    $('#adaptive-load-arrow').toggleClass('rotate-arrow');

    sortRecordTableData('data-date-of-download', '#adaptive-load-arrow');
});

$('#amount-arrow-parent').click(function() {
    $('#amount-arrow').toggleClass('rotate-arrow');
    
    function delaySorting() {
        sortRecordTableData('data-downloads', '#amount-arrow');
    }

    setTimeout(delaySorting, 350);
});

$('#adaptive-amount-arrow-parent').click(function() {
    $('#adaptive-amount-arrow').toggleClass('rotate-arrow');
    
    sortRecordTableData('data-downloads', '#adaptive-amount-arrow');
});

//Sorting function for reports table.
//Also in this function we need to make 
//positioning of shadow boxes for report table rows
function sortRecordTableData(context, parentId) {
    var $sortTable = $('div.row-items'),
        $sortRows = $sortTable.children('.filterable-element');
    
    $sortRows.sort(function(a, b) {
        if (parentId == '#load-arrow' || parentId == '#adaptive-load-arrow') {
            var a = a.getAttribute(context).split('-');
            var b = b.getAttribute(context).split('-');
            a = (parseInt(a[0]) * 360) + (parseInt(a[1]) * 30) + parseInt(a[2]);
            b = (parseInt(b[0]) * 360) + (parseInt(b[1]) * 30) + parseInt(b[2]);
        } 
        
        if (parentId == '#amount-arrow' || parentId == '#adaptive-amount-arrow') { 
            var a = a.getAttribute(context);
            var b = b.getAttribute(context);
            a = parseInt(a);
            b = parseInt(b);
        }

        if ($(parentId).hasClass('rotate-arrow')) {
            if (a > b) {

                return -1;
            }

            if (a < b) {

                return 1;
            }

            return 0;
        } else {
            if (a < b) {

                return -1;
            }

            if (a > b) {

                return 1;
            }
            
            return 0;
        }
    });

    $sortRows.detach().appendTo($sortTable);
}

//Owl-carousels
var owlPartners = $('.owl-partners');
var owlMainPage = $('.owl-main-page');
var owlCompany = $('.owl-company');
var owlNewsPage = $('.owl-news-page');

var slideMargin;

if ($(window).width() > 600) {
    slideMargin = -100;
} else if ($(window).width() < 480) {
    slideMargin = -150;
} else if ($(window).width() < 600) {
    slideMargin = -50;
} 

owlPartners.owlCarousel({
    loop: true,
    dots: false,
    dotData: false,
    dotsData: false,
    margin: slideMargin,
    responsive: {
        0:{
            items:1
        },
        480:{
            items:2
        },
        1000: {
            items:3
        },
        1350:{
            items:4
        }
    }
});

$('.prev-partners-btn').click(function() {
    owlPartners.trigger('prev.owl.carousel');
});

$('.next-partners-btn').click(function() {
    owlPartners.trigger('next.owl.carousel');
});

owlMainPage.owlCarousel({
    loop: true,
    dots: false,
    dotData: false,
    dotsData: false,
    margin: 0,
    responsive: {
        0:{
            items:1
        },
        730: {
            items:2
        },
        900: {
            items:2
        },
        1200: {
            items:3
        },
        1500: {
            items:4
        }
    }
});

//Owl carousel for company and news pages
function callSlider(context) {
    owlCompany.owlCarousel({
        loop: true,
        dots: false,
        dotData: false,
        dotsData: false,
        margin: 35,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            }
        }
    });

    owlNewsPage.owlCarousel({
        loop: false,
        dots: false,
        dotData: false,
        dotsData: false,
        margin: 0,
        responsive: {
            0:{
                items:1
            },
            480:{
                items:2
            },
            730: {
                items:3
            }
        }
    });
}

$(window).ready(function() {
    if ($(window).width() < 900) {
        callSlider();
    }
});

// Ajax calls for switching cities
// $(function(){
//     $('#toggle-town-sub-header').click(function(e) {
//         var thisForm = $(this);
//         //Prevent the default form action
//         e.preventDefault();
//         //Hide the form
//         $.ajax({
//             type: 'POST',
//             url: thisForm.attr("action"),
//             data: thisForm.serialize(),

//             //Wait for a successful response
//             success: function(data) {
//                 alert(data);
//                 $('.callback-phone').load(location.href + " .callback-phone");
//                 $('.callback-phone-burger').load(location.href + " .callback-phone-burger");
//                 $('.batumi').load(location.href + " .batumi");
//                 $('.tbilisi').load(location.href + " .tbilisi");
//                 $('.batumi-modal').load(location.href + " .batumi-modal");
//                 $('.tbilisi-modal').load(location.href + " .tbilisi-modal");
//                 $('.contacts-tel-items').load(location.href + " .contacts-tel-items");
//                 $('.town-slider-checked').toggleClass('move-city-toggler');
//                 $('.town-slider-checked-modal').toggleClass('move-city-toggler-burger');
//             }
//         });
//     });

//     $('#toggle-town-burger').click(function(e) {
//         var thisForm = $(this);
//         //Prevent the default form action
//         e.preventDefault();
//         //Hide the form
//         $.ajax({
//             type: 'POST',
//             url: thisForm.attr("action"),
//             data: thisForm.serialize(),

//             //Wait for a successful response
//             success: function(data) {
//                 $('.callback-phone').load(location.href + " .callback-phone");
//                 $('.callback-phone-burger').load(location.href + " .callback-phone-burger");
//                 $('.batumi-modal').load(location.href + " .batumi-modal");
//                 $('.tbilisi-modal').load(location.href + " .tbilisi-modal");
//                 $('.batumi').load(location.href + " .batumi");
//                 $('.tbilisi').load(location.href + " .tbilisi");
//                 $('.contacts-tel-items').load(location.href + " .contacts-tel-items");
//                 $('.town-slider-checked-modal').toggleClass('move-city-toggler-burger');
//                 $('.town-slider-checked').toggleClass('move-city-toggler');
//             }
//         });
//     });
// });