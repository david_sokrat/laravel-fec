@extends('backend.layouts.backend')


@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">


                    <ol class="breadcrumb bread-primary ">

                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>

                        <li><a href="#">{{tc('details')}}</a></li>

                        <li class="active">Mail List</li>


                    </ol>



                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" > <br>








                        <div class="table-responsive">
                            <a href="" class="btn btn-success btn-xs" id="axalitranslate" data-toggle="modal"
                               data-target=".modal-add"> <i class="material-icons">add_circle</i> </a>
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">Id</th>
                                    <th class="column-title">{{tc('name')}}</th>
                                    <th class="column-title">Mail</th>
                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>




                              @foreach($mails as $mail)
                                <tr class="even pointer">
                                    <td class="">{{$mail->id}}</td>
                                    <td class="">{{$mail->name}}</td>
                                    <td class="">{{$mail->mail}}</td>
                                    <td class="">

                                        <a href="" class="delmail" style="color: #d30004;"
                                           data-id="{{$mail->id}}" data-toggle="modal"
                                           data-target=".modal-delmail"><i
                                                    class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                                  @endforeach









                                </tbody>
                            </table>
                        </div>







                        <div class="modal fade modal-delmail" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel2">{{tc('remove')}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('cms.mail.remove')}}" method="post">
                                            <input type="hidden" name="delmailid" id="delmailid"/>
                                            {{ csrf_field() }}
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{tc('close')}}</button>
                                            <button type="submit" class="btn btn-primary btn-danger">{{tc('remove')}}</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="modal fade modal-add" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel2"> </h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-label-left" action="{{route('cms.mail.add')}}" method="post">


                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('name')}}</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" name="name"  placeholder="Add Name" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('mail')}}</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control" name="mail"  placeholder="Add mail" required>
                                                </div>
                                            </div>





                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="submit" class="btn btn-success">{{tc('save')}}</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section('scripts')

    <script>
        $(document).ready(function () {
            $('.delmail').click(function (e) {
                e.preventDefault();
                var that = this;
                $('#delmailid').val($(this).data('id'));
            });
        });
    </script>



    @endsection
