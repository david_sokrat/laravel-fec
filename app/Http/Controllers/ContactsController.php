<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\City;

class ContactsController extends Controller
{
    public function changeContacts(Request $request)
    {
        $city = $request->cur_city;
        
        if ($city === 'tbilisi') { 
            Session::put('city', 'batumi');
        } else {
            Session::put('city', 'tbilisi');
        }

        return redirect()->back();
    }
}
