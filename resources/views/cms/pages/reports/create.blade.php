@extends('cms.layouts.backend')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('reports.index') }}">{{ $translations['cms_sidebar_nav_reports'] }}</a></li>
                        <li class="active">{{ $translations['cms_sidebar_nav_creating'] }}</li>
                    </ol>

                    @include('partials._messages')

                    <div class="x_content"><br>
                        {!! Form::open(['route' => ['reports.store'], 'files' => true, 'id' => 'demo-form2', 'class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '']) !!}
                            {{ csrf_field() }}

                            @php $hash = strtotime(date('Y:m:d h:i:s')) @endphp
                            {{ Form::hidden('trans_title_key', 'report.title' . $hash, array()) }}
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in">
                                        <div class="form-group"> 
                                            {{ Form::label('user_id', $translations['cms_report_client'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="user_id" class="form-control col-md-7 col-xs-12" style="border-radius: 3px;">

                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group"> 
                                            {{ Form::label('status', $translations['cms_report_status'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12" style="padding-top: 10px;">
                                                {{ Form::radio('status', 0, array()) }} {{ $translations['cms_report_progress'] }}
                                                {{ Form::radio('status', 1, array()) }} {{ $translations['cms_report_complete'] }}
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group"> 
                                            {{ Form::label('title', $translations['cms_report_period'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12" style="display: flex;">
                                                <div style="width: 50%;">
                                                    {{ Form::label('from', $translations['cms_report_from'], array()) }}
                                                    {{ Form::date('from', null, array('class' => 'form-control col-md-6 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'firstText_', 'required' => '')) }}
                                                </div>
                                                <div style="width: 50%;">
                                                    {{ Form::label('to', $translations['cms_report_to'], array()) }}
                                                    {{ Form::date('to', null, array('class' => 'form-control col-md-6 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'firstText_', 'required' => '')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group"> 
                                            {{ Form::label('date', $translations['cms_item_date'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{ Form::date('date', null, array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'firstText_', 'required' => '')) }}
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group"> 
                                            {{ Form::label('path_to_excel', $translations['cms_report_excel'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                                {{ Form::file('path_to_excel', array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'required' => '')) }}
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            {{ Form::label('path_to_docs', $translations['cms_report_word'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                                {{ Form::file('path_to_docs', array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'required' => '')) }}
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            {{ Form::label('path_to_pdf', $translations['cms_report_pdf'], array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
                                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                                {{ Form::file('path_to_pdf', array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'required' => '')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    {{ Form::submit($translations['cms_tab_submit'], array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop