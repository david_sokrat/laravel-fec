<section id="useful-docs">
    <div class="wrapper">
        <div class="wrap-left-right">
            <div class="left-side">
                <h2>{{ $translations['useful_docs_header'] }}</h2>
                <div class="docs-items">
                    <a download href="/files/avto.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">ავტომობილის იჯარა</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/ipoteka.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">იპოთეკის ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/mibarebis.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">მიბარების ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/nardobis.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">ნარდობის ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/seshi.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">სესხის ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/tvirtis.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">ტვირთის გადაზიდვის ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/franchize.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">ფრენშაიზინგის ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                    <a download href="/files/rukebis.docx">
                        <div class="docs-item">
                            <img src="{{ asset('img/docs.svg') }}" alt="Word file to upload" />
                            <div>
                                <span class="download-link">ჩუქების ხელშეკრულება</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="right-side">
                <h2>{{ $translations['usful_links'] }}</h2>
                <div class="links-items">
                    <a class="links-item1" target="_blank" href="https://www.rs.ge//">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child1">rs.ge</span>
                                <span class="link-descr">{{ $translations['usful_tax'] }}</span>
                            </div>
                        </div>
                    </a>
                    <a target="_blank" href="https://www.saras.gov.ge/">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child2">saras.gov.ge</span>
                                <span class="link-descr">{{ $translations['usful_acounting'] }}</span>
                            </div>
                        </div>
                    </a>
                    <a class="links-item3" target="_blank" href="https://matsne.gov.ge/">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child3">matsne.gov.ge</span>
                                <span class="link-descr">{{ $translations['useful_news'] }}</span>
                            </div>
                        </div>
                    </a>
                    <a target="_blank" href="https://mof.ge/">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child4">mof.gov.ge</span>
                                <span class="link-descr">{{ $translations['usful_ministry'] }}</span>
                            </div>
                        </div>
                    </a>
                    <a class="links-item5" target="_blank" href="https://www.nbg.gov.ge">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child5">nbg.gov.ge</span>
                                <span class="link-descr">{{ $translations['usful_bank'] }}</span>
                            </div>
                        </div>
                    </a>
                    <a target="_blank" href="https://reportal.ge/">
                        <div class="links-item">
                            <img src="{{ asset('img/grid-world.svg') }}" alt="Pdf file to upload" />
                            <div>
                                <span class="download-link child6">reportal.ge</span>
                                <span class="link-descr">{{ $translations['usful_portal'] }}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>