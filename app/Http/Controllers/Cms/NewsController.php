<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Article;
use App\Navigation;
use App\TranslationContent;
use App\Locales;
use App\Admin;
use Session;
use Image;
use View;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Article::orderBy('id', 'desc')->paginate(6);
        $admin = Admin::find(Session::get('admin_profile_id'));

        return view('cms/pages/news/news')->withNews($news)->withAdmin($admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locales = Locales::all();

        View::share('locales', $locales);
        $admin = Admin::find(Session::get('admin_profile_id'));
        Session::put('admin_obj', $admin);

        return view('cms/pages/news/create')->withAdmin($admin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //validate the data
        $this->validate($request, array(
            'date' => 'required',
            'image_name' => 'required',
        ));

        //store in the database
        $new = new Article;
        $new->date = $request->input('date');
        $new->title = $request->input('trans_title_key');
        $new->body = $request->input('trans_body_key');
        $new->slug = $request->input('trans_slug_key');

        if ($request->file('image_name') == null) {
            $image = "";
        } else {
            $image = $request->file('image_name');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = storage_path('app/public/img/' . $filename);
            Image::make($image->getRealPath())->save($location);
            $new->image_name = $filename;
        }

        $new->save();

        $locales = Locales::all();

        View::share('locales', $locales);

        $loc = array('ru', 'en', 'ka', 'tr');
            
        for ($i = 0; $i < 4; $i++) {
            //validate the data
            $this->validate($request, array(
                'title' . $loc[$i] => 'required|max:255',
                'body' . $loc[$i] => 'required',
                'slug' . $loc[$i] => 'required|alpha_dash|min:5|max:255|unique:articles,slug',
            ));

            $translations_contents = new TranslationContent;

            $translations_contents->trans_title_key = $request->input('trans_title_key');
            $translations_contents->title_value = $request->input('title' . $loc[$i]);
            $translations_contents->trans_body_key = $request->input('trans_body_key');
            $translations_contents->body_value = $request->input('body' . $loc[$i]);
            $translations_contents->locale = $loc[$i];
            $translations_contents->trans_slug_key = $request->input('trans_slug_key');
            $translations_contents->slug = $request->input('slug' . $loc[$i]);
            $translations_contents->article_id = $new->id;

            $translations_contents->save();
        }

        Session::put('success', trans('validation.article-success'));

        //redirect to other page;
        return view('cms/pages/news/index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find(Session::get('admin_profile_id'));
        $loc = array('ru', 'en', 'ka', 'tr');
        
        $new = Article::find($id);
        $locales = Locales::all();
        $title = DB::table('translations_contents')->where('article_id', $id)->pluck('title_value');
        $body = DB::table('translations_contents')->where('article_id', $id)->pluck('body_value');
        $slug = DB::table('translations_contents')->where('article_id', $id)->pluck('slug');

        View::share('locales', $locales);

        return view('cms/pages/news/edit')
        ->withNew($new)
        ->withTitle($title)
        ->withSlug($slug)
        ->withBody($body)
        ->withAdmin($admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate the data
        $this->validate($request, array(
            'date' => 'required',
            'image_name' => 'required',
        ));

        $new = Article::find($id);
        
        $new->date = $request->input('date');

        if ($request->file('image_name') == null) {
            $image = "";
        } else {
            $image = $request->file('image_name');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = storage_path('app/public/img/' . $filename);
            Image::make($image->getRealPath())->save($location);
            $new->image_name = $filename;
        }

        $new->save();

        $locales = Locales::all();

        View::share('locales', $locales);

        $loc = array('ru', 'en', 'ka', 'tr');
            
        for ($i = 0; $i < 4; $i++) {
            //validate the data
            $this->validate($request, array(
                'title' . $loc[$i] => 'required|max:255',
                'body' . $loc[$i] => 'required',
                'slug' . $loc[$i] => 'required|alpha_dash|min:5|max:255|unique:articles,slug',
            ));

            $translations_contents = DB::table('translations_contents')
                ->where('article_id', $id)
                ->where('locale', $loc[$i])
                ->update(['title_value' => $request->input('title' . $loc[$i])]);
                
            $translations_contents = DB::table('translations_contents')
                ->where('article_id', $id)
                ->where('locale', $loc[$i])
                ->update(['body_value' => $request->input('body' . $loc[$i])]);

            $translations_contents = DB::table('translations_contents')
                ->where('article_id', $id)
                ->where('locale', $loc[$i])
                ->update(['slug' => $request->input('slug' . $loc[$i])]);
        }

        Session::put('success', "The article was successfully updated!");

        //redirect with flash data to news.show
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $new = Article::find($id);
        $trans = DB::table('translations_contents')->where('article_id', $id);

        $new->delete();
        $trans->delete();

        Session::put('success', 'The article was successfully deleted!');

        return redirect()->route('news.index');
    }
}
