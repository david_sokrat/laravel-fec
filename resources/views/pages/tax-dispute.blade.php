@extends('main')

@section('title', '| Tax Dispute')

@section('stylesheets')
    <link href="{{ asset('css/tax-dispute.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = true;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('canvas')
    <canvas id="canvas4"></canvas>
@endsection

@section('content')
    @include('partials.tax-dispute._tax-dispute-promo') 
    @include('partials.tax-dispute._tax-dispute-text-content') 
    @include('partials.tax-dispute._tax-order-form') 
@endsection