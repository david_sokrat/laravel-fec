@extends('backend.layouts.backend')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                    </ol>
                    <div class="x_content"></div>
                </div>
            </div>
        </div>
    </div>
@stop

