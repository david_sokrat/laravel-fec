let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/parsley.min.js', 'public/js')
    .js('resources/assets/js/cms.js', 'public/js')
    .combine([
        'resources/assets/js/jquery-3.3.1.js',
        'resources/assets/js/owl.carousel.js',
        'resources/assets/js/debounce-v1.1.js',
        'resources/assets/js/jquery-easing.js',
        'resources/assets/js/three.min.js',
        'resources/assets/js/OrbitControls.js',  
        'resources/assets/js/Detector.js', 
        'resources/assets/js/Projector.js',
        'resources/assets/js/TTFLoader.js', 
        'resources/assets/js/opentype.min.js', 
        'resources/assets/js/scripts.js', 
        'resources/assets/js/vivus.js', 
        'resources/assets/js/googleMapInit.js',
        'resources/assets/js/bootstrap.js',
        'resources/assets/js/ckeditor.js'
    ], 'public/js/app.js')
    .less('resources/assets/less/reset.less', 'public/css')
    .less('resources/assets/less/normalize.less', 'public/css')
    .less('resources/assets/less/parsley.less', 'public/css')
    .less('resources/assets/less/app.less', 'public/css')
    .less('resources/assets/less/news.less', 'public/css')
    .less('resources/assets/less/cms.less', 'public/css')
    .less('resources/assets/less/academy.less', 'public/css')
    .less('resources/assets/less/article.less', 'public/css')
    .less('resources/assets/less/audit.less', 'public/css')
    .less('resources/assets/less/company.less', 'public/css')
    .less('resources/assets/less/contacts.less', 'public/css')
    .less('resources/assets/less/finance.less', 'public/css')
    .less('resources/assets/less/legal.less', 'public/css')
    .less('resources/assets/less/profile.less', 'public/css')
    .less('resources/assets/less/tax-dispute.less', 'public/css')
    .less('resources/assets/less/media.less', 'public/css')
    .less('resources/assets/less/owl.carousel.less', 'public/css')
    .less('resources/assets/less/owl.theme.default.less', 'public/css')
    .version();