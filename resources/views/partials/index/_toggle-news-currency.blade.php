<section id="toggle-news-currency">
    <div class="wrapper">
        <div class="toggle-bar">
            <h2 class="news-toggle active-slider">{{ $translations['currency_news'] }}</h2>
            <label class="switch">
                <input type="checkbox">
                <span class="slider round"></span>
            </label>
            <h2 class="currency-toggle">{{ $translations['currency_currency'] }}</h2>
        </div>
        <div class="owl-carousel owl-theme owl-loaded news-main-wrapper">
            <div class="owl-stage-outer owl-main-page">
                <div class="news-block owl-stage">                                            

                    @foreach ($news as $new)

                        <div class="year{{ substr($new->date, 0, 4) }} news-item owl-item">
                            <div id="item1" class="item" style="background: url( {{ asset('storage/public/img/' . $new->image_name) }}) center top no-repeat; background-size: cover;"></div>
                            <div class="news-descr">
                                <a href="{{ route('show.new', ['lang' => Session::get('locale'), 'slug' => $translations_contents[$new->slug]]) }}"><span class="text-link">{{ $translations_contents[$new->title] }}<span></a>
                                <span class="date-of-news">{{ $new->date }}</span>
                            </div>
                        </div>

                    @endforeach        
                    
                </div>
            </div>
        </div>
        <div class="currency-main-wrapper" style="display: none;">
            @php $client = new SoapClient('http://nbg.gov.ge/currency.wsdl'); @endphp
            <div class="currency-item currency-header">
                <span class="currency-descr">Description</span>
                <span class="currency-value">Amount</span>
                <span class="currency-rate">Rate</span>
                <span class="currency-change">Change</span>
                <span class="currency-date">Date</span>
            </div>
            <div class="currency-item">
                <span class="currency-descr"> {{ $client->GetCurrencyDescription('RUB') }} </span>
                <span class="currency-value"> {{ $client->GetCurrency('RUB') }} </span>
                <span class="currency-rate"> {{ $client->GetCurrencyRate('RUB') }} </span>
                <span class="currency-change"> {{ $client->GetCurrencyChange('RUB') }} </span>
                <span class="currency-date"> {{ date('Y-m-d') }} </span>
            </div>
            <div class="currency-item">
                <span class="currency-descr"> {{ $client->GetCurrencyDescription('USD') }} </span>
                <span class="currency-value"> {{ $client->GetCurrency('USD') }} </span>
                <span class="currency-rate"> {{ $client->GetCurrencyRate('USD') }} </span>
                <span class="currency-change"> {{ $client->GetCurrencyChange('USD') }} </span>
                <span class="currency-date"> {{ date('Y-m-d') }} </span>
            </div>
            <div class="currency-item">
                <span class="currency-descr"> {{ $client->GetCurrencyDescription('USD') }} </span>
                <span class="currency-value"> {{ $client->GetCurrency('USD') }} </span>
                <span class="currency-rate"> {{ $client->GetCurrencyRate('USD') }} </span>
                <span class="currency-change"> {{ $client->GetCurrencyChange('USD') }} </span>
                <span class="currency-date"> {{ date('Y-m-d') }} </span>
            </div>
            <div class="currency-item currency-item-last">
                <span class="currency-descr"> {{ $client->GetCurrencyDescription('USD') }} </span>
                <span class="currency-value"> {{ $client->GetCurrency('USD') }} </span>
                <span class="currency-rate"> {{ $client->GetCurrencyRate('USD') }} </span>
                <span class="currency-change"> {{ $client->GetCurrencyChange('USD') }} </span>
                <span class="currency-date"> {{ date('Y-m-d') }} </span>
            </div>
        </div>
    </div>
</section>