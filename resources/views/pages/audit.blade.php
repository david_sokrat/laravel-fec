@extends('main')

@section('title', '| Audit')

@section('stylesheets')
    <link href="{{ asset('css/audit.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = true;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('canvas')
    <canvas id="canvas3"></canvas>
@endsection

@section('content')
    @include('partials.audit._tax-dispute-promo') 
    @include('partials.audit._tax-dispute-text-content') 
    @include('partials.audit._tax-order-form') 
@endsection