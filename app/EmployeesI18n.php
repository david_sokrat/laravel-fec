<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EmployeesI18n
 * @package App
 */
class EmployeesI18n extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'employees_i18n';

    /**
     * No timestamps.
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [ ];

    /**
     * @return mixed
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Content\Employees', 'id', 'id');
    }
}