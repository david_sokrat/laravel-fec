<section id="employee-galery">
    <div class="carousel-wrapper owl-carousel owl-theme owl-loaded">
        <div class="wrapper owl-company owl-stage-outer">
            <ul class="employee-galery-items owl-stage">
                @foreach ($employees as $empl)
                    @php $image = App\Employees::find($empl->id); @endphp
                    <li class="employee-galery-item owl-item">
                        <div class="photo" style="background: url('/img/{{ $image->image }}') top center no-repeat;background-size: cover;"></div>
                        <div class="employee-galery-item-descr">
                            <h4>{{ $empl->firstname . ' ' . $empl->lastname }}</h4>
                            <p class="speciality">{{ $empl->job_title }}</p>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>