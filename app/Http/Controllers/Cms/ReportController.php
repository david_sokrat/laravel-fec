<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Support\Facades\DB;
use App\TranslationContent;
use App\Report;
use App\User;
use Session;
use View;
use App\Admin;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::find(Session::get('admin_profile_id'));
        Session::put('admin_obj', $admin);

        $users = User::all();
        $reports = Report::orderBy('id', 'desc')->paginate(5);
        $dates = array();
        $dayData = array();
        $curDate = str_replace("-", "", date('Y-m-d'));

        for ($i = 0; $i < count($reports); $i++) {
            $dates[$i] = str_replace("-", "", $reports[$i]->date);
            
            if ($dates[$i] == $curDate) {
                $dates[$i] = 'сегодня';
                $reports[$i]['date'] = $dates[$i];
            }

            if ($dates[$i] == $curDate - 1) {
                $dates[$i] = 'вчера';
                $reports[$i]['date'] = $dates[$i];
            }
        }

        View::share('dates', $dates);

        return view('cms/pages/reports/reports')
        ->withReports($reports)
        ->withsUsers($users)
        ->withAdmin($admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin = Admin::find(Session::get('admin_profile_id'));
        
        $users = User::all();

        return view('cms/pages/reports/create')->withUsers($users)->withAdmin($admin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::all();

        $excel = strtolower($request->path_to_excel->getClientOriginalExtension());
        $word = strtolower($request->path_to_docs->getClientOriginalExtension());
        $pdf = strtolower($request->path_to_pdf->getClientOriginalExtension());

        if (($excel == 'xlsx' || $excel == 'xls') && ($word == 'docx' || $word == 'doc') && ($pdf == 'pdf')) {
            //validate the data
            $this->validate($request, array(
                'date' => 'required',
                'user_id' => 'required|integer',
                'path_to_excel' => 'required',
                'path_to_docs' => 'required',
                'path_to_pdf' => 'required'
            ));
        } else {
            Session::flash('error', 'Wrong file extension. Please use Word, Excel and Pdf files!');

            return view('cms/pages/reports/create')->withUsers($users);
        }

        //store in the database
        $report = new Report;

        if ($request->file('path_to_excel') == null) {
            $path_excel = "";
        } 
        
        if ($request->file('path_to_pdf') == null) {
            $path_pdf = "";
        }
        
        if ($request->file('path_to_docs') == null) {
            $path_docs = "";
        }
              
        $path_excel = $request->file('path_to_excel')->store('public/files');
        $path_docs = $request->file('path_to_docs')->store('public/files');
        $path_pdf = $request->file('path_to_pdf')->store('public/files');

        $report->status = $request->status;
        $report->date = $request->date;
        $report->user_id = $request->user_id;
        $report->from = $request->from;
        $report->to = $request->to;
        $report->title = $request->trans_title_key;
        $report->path_to_excel = $path_excel;
        $report->path_to_docs = $path_docs;
        $report->path_to_pdf = $path_pdf;
        
        $report->save();

        $locales = array('ru', 'en', 'ka', 'tr');
        $titles = array(
            'Отчёт за период ' . $request->from . ' - ' . $request->to . '.', 
            'Reporting period ' . $request->from . ' - ' . $request->to . '.',
            'რეპორტინგის პერიოდი ' . $request->from . ' - ' . $request->to . '.',
            'Gulishihalalamala ' . $request->from . ' - ' . $request->to . '.'
        );

        for ($i = 0; $i < 4; $i++) {
            $translations_contents = new TranslationContent;
            $translations_contents->title_value = $titles[$i];
            $translations_contents->trans_title_key = $request->trans_title_key;
            $translations_contents->locale = $locales[$i];
            $translations_contents->report_id = $report->id;

            $translations_contents->save();
        }

        Session::put('success', 'The report was successfully saved!');

        //redirect to other page;
        return redirect()->route('reports.index')->with('users', $users);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::find(Session::get('admin_profile_id'));

        $reports = Report::orderBy('id', 'desc')->paginate(5);
        $dates = array();
        $dayData = array();
        $curDate = str_replace("-", "", date('Y-m-d'));

        for ($i = 0; $i < count($reports); $i++) {
            $dates[$i] = str_replace("-", "", $reports[$i]->date);
            
            if ($dates[$i] == $curDate) {
                $dates[$i] = 'сегодня';
                $reports[$i]['date'] = $dates[$i];
            }

            if ($dates[$i] == $curDate - 1) {
                $dates[$i] = 'вчера';
                $reports[$i]['date'] = $dates[$i];
            }
        }

        View::share('dates', $dates);

        return redirect()->route('reports.show')->withReports($reports)->withAdmin($admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find(Session::get('admin_profile_id'));

        $report = Report::find($id);
        $users = User::all();

        return view('cms/pages/reports/edit')->withReport($report)->withUsers($users)->withAdmin($admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::all();
        $report = Report::find($id);

        if ($request->file('path_to_excel') == null) {
            $path_excel = $request->hidden_excel;
            $excel = 'xlsx';
        } else {
            $excel = strtolower($request->path_to_excel->getClientOriginalExtension());
            $path_excel = $request->file('path_to_excel')->store('public/files');
            $report->path_to_excel = $path_excel;
        }
        
        if ($request->file('path_to_pdf') == null) {
            $path_pdf = $request->hidden_pdf;
            $pdf = 'pdf';
        } else {
            $pdf = strtolower($request->path_to_pdf->getClientOriginalExtension());
            $path_pdf = $request->file('path_to_pdf')->store('public/files');
            $report->path_to_pdf = $path_pdf;
        }
        
        if ($request->file('path_to_docs') == null) {
            $path_docs = $request->hidden_docs;
            $word = 'docx';
        } else {
            $word = strtolower($request->path_to_docs->getClientOriginalExtension());
            $path_docs = $request->file('path_to_docs')->store('public/files');
            $report->path_to_docs = $path_docs;
        }

        if (($excel == 'xlsx' || $excel == 'xls') && ($word == 'docx' || $word == 'doc') && ($pdf == 'pdf')) {
            //validate the data
            $this->validate($request, array(
                'date' => 'required',
                'user_id' => 'required|integer'
            ));

            Session::put('success', 'The report was successfully updated!');
        } else {
            Session::flash('error', 'Wrong file extension. Please use Word, Excel and Pdf files!');

            return view('cms/pages/reports/edit')->withReport($report)->withUsers($users);
        }

        //Save the data to the database   
        $report->status = $request->status;
        $report->date = $request->date;
        $report->user_id = $request->user_id;
        $report->from = $request->from;
        $report->to = $request->to;
        
        $report->save();

        $locales = array('ru', 'en', 'ka', 'tr');
        $titles = [
            ['title_value' => 'Отчёт за период ' . $request->from . ' - ' . $request->to . '.'], 
            ['title_value' => 'Reporting period ' . $request->from . ' - ' . $request->to . '.'],
            ['title_value' => 'რეპორტინგის პერიოდი ' . $request->from . ' - ' . $request->to . '.'],
            ['title_value' => 'Gulishihalalamala ' . $request->from . ' - ' . $request->to . '.']
        ];

        for ($i = 0; $i < 4; $i++) {
            $translations_contents = DB::table('translations_contents')->where('report_id', $id)->update($titles[$i]);
        }

        //redirect to other page;
        return redirect()->route('reports.index')->withUsers($users);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $report = Report::find($id);
        $trans = DB::table('translations_contents')->where('article_id', $id);

        $report->delete();
        $trans->delete();

        Session::put('success', 'The report was successfully deleted!');

        return redirect()->route('reports.index');
    }
}
