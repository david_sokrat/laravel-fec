<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationRel extends Model
{
    protected $table = 'trans_content_rel';
}
