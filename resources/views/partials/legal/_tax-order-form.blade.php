<section id="tax-order-form" class="form-dispute-page">
    <div class="wrapper">
        <h2>{{ $translations['audit_form_h2_title'] }}</h2>
        <form id="legal-form" action="{{ route('audit-form', Session::get('locale')) }}" method="POST">
            {{ csrf_field() }}
            <label for="form">{{ $translations['audit_form_label'] }}</label>
            <div class="input-fields">
                <input type="hidden" name="form_name" value="Legal page form">
                <input type="text" placeholder="{{ $translations['audit_form_company'] }}" name="company" class="company-name">
                <input type="text" placeholder="{{ $translations['audit_form_phone'] }}" name="phone" class="company-phone">
                <input type="text" placeholder="{{ $translations['audit_form_company_type'] }}" name="type-of-maintanance" class="company-type-of-maintanance">
                <input type="text" placeholder="{{ $translations['audit_form_company_adress'] }}" name="adress" class="company-adress">
                <input type="text" placeholder="{{ $translations['audit_form_company_email'] }}" name="email" class="company-email">
                <input type="text" placeholder="{{ $translations['audit_form_company_programm'] }}" name="pc-programm" class="company-pc-programm">
                <textarea name="client_message" class="client-message" placeholder="{{ $translations['audit_form_message'] }}"></textarea>
            </div>
            <div class="btn-wrap">
                <button type="submit" class="btn-tax-dispute" form="legal-form"><span>{{ $translations['audit_form_submit_btn'] }}</span></button>
            </div>
        </form>
    </div>
</section>