<header id="header" class="main-page-header">
    <div class="wrapper-header">
        <div class="logo">
            <a href="{{ route('home', ['lang' => Session::get('locale')]) }}">
                <img src="{{ asset('img/fec_logo.svg') }}" alt="FEC logo" />
            </a>
        </div>
        <div class="align-header">
            <div class="header-menu">
                <nav>
                    <ul>
                        <li>
                            <a class="{{ Route::currentRouteName() == "home" ? "active-link" : "" }}" href="{{ route('home', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_perf'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "company" ? "active-link" : "" }}" href="{{ route('company', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_company'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "articles" ? "active-link" : "" }}" href="{{ route('articles', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_news'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "contacts" ? "active-link" : "" }}" href="{{ route('contacts', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_contacts'] }}</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="callback-phone">
                <a href="{{ Session::get('city') === 'batumi' ? "tel:+995 422 27 77 11" : "tel:+995 555 55 27 77" }}">
                    @if(Session::get('city') == 'batumi')
                        +995 422 27 77 11
                    @else
                        +995 555 55 27 77
                    @endif
                </a>
            </div>
        </div>
        <div class="client-profile">
            @if(Auth::guest())
                <a href="#"><img src="{{ asset('img/user-shape.svg') }}" />{{ $translations['header_nav_profile'] }}</a>
            @endif
            @if(Auth::check())
                <a href="{{ route('logout', Session::get('locale')) }}"><img src="{{ asset('img/logout.svg') }}" />{{ $translations['header_nav_logout'] }}</a>
                @if(Route::currentRouteName() != 'profile')
                    <a id="link-to-user-account" href="{{ route('reports', Session::get('locale')) }}" style="right: 115px; position: absolute !important; border: none !important; border-radius: none !important; line-height: 0; padding: 0; width: 195px; font-size: 0.7rem;">{{ $translations['header_nav_account'] }}</a>
                @endif
            @endif
        </div>
    </div>
</header>