<section id="company-offers">
    <div class="wrapper">
        <div class="flex-wrap">
            <h2>{{ $translations['offer_we'] }}</h2>
            <nav class="offers">
                <ul>
                    <li class="first-li-offers">
                        <button class="tablinks active-form-tab" onclick="openNewTab(event, 'audit-form')">{{ $translations['offer_audit'] }}</button>
                    </li>
                    <li>
                        <button class="tablinks" onclick="openNewTab(event, 'finance-form')">{{ $translations['offer_acounting'] }}</button>
                    </li>
                    <li>
                        <button class="tablinks" id="default-open" onclick="openNewTab(event, 'guide-form')">{{ $translations['offer_bussiness'] }}</button>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>