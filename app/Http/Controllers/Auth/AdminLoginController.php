<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
use Session;
use Illuminate\Support\Facades\DB;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm() 
    {
        return view('admin/admin-login');
    }

    public function login(Request $request) 
    {
        $profileId = substr(DB::table('admins')->where('email', $request->email)->pluck('id'), 1, -1);
        Session::put('admin_obj', $profileId);

        //Validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        //Attempt to log the user in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            //If successfull, then redirect to their intended location

            Session::put('success', trans('validation.admin-logged-success'));

            return redirect()->intended(route('cms.dashboard'));
        }

        //If unsuccessfull, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Session::put('success', 'Admin was successfully logged out!');
        Session::forget('admin_porfile_id');
        Auth::guard('admin')->logout();

        return redirect('/');
    }
}
