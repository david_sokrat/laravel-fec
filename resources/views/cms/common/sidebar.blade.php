<div class="profile clearfix">
    <div class="profile_pic">
        @if('')
            <img src="{{ asset('storage/public/img/avatar.svg') }}" alt="..." class="img-circle profile_img" style="width: 40px; height: 40px;">
        @else
            <img src="{{ asset('storage/public/img/avatar.svg') }}" alt="..." class="img-circle profile_img" style="width: 40px;">
        @endif
    </div>
    <div class="profile_info">
        <a href="{{ route('cms.profile') }}">
            <h2>Имя</h2>
            <span>Должность</span>
        </a>
    </div>
</div>

<br />

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>{{ $translations['cms_sidebar_nav_title'] }}</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i>{{ $translations['cms_sidebar_nav_home'] }}<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('cms.dashboard') }}">{{ $translations['cms_sidebar_nav_dashboard'] }}</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-sort-amount-desc"></i>{{ $translations['cms_sidebar_nav_reports'] }}<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('reports.create') }}">{{ $translations['cms_sidebar_nav_add_report'] }}</a></li>
                    <li><a href="{{ route('reports.index') }}">{{ $translations['cms_sidebar_nav_all_reports'] }}</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-newspaper-o"></i>{{ $translations['cms_sidebar_nav_title_news'] }}<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('news.create') }}">{{ $translations['cms_sidebar_nav_add_new'] }}</a></li>
                    <li><a href="{{ route('news.index') }}">{{ $translations['cms_sidebar_nav_all_news'] }}</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-language"></i>{{ $translations['cms_sidebar_nav_lang'] }}<span class="fa fa-chevron-down"></span></a>
                {!! Form::open(['route' => 'cms.language.change']) !!}
                    {{-- {!! Form::select('lang', config('translatable.locales'), app()->getLocale(), ['id' => 'language-select-sub-header']) !!} --}}
                    <ul class="nav child_menu">
                        <li>
                            <a><button class="language-select-cms lang-btn" type="submit" name="lang" value="ru">RU</button></a>
                        </li>
                        <li>
                            <a><button class="language-select-cms lang-btn" type="submit" name="lang" value="en">EN</button></a>
                        </li>
                        <li>
                            <a><button class="language-select-cms lang-btn" type="submit" name="lang" value="ka">KA</button></a>
                        </li>
                        <li>
                            <a><button class="language-select-cms lang-btn" type="submit" name="lang" value="tr">TR</button></a>
                        </li>
                    </ul>
                {!! Form::close() !!}
            </li>
        </ul>
    </div> 
</div>

<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Logs">
        <i class="material-icons">bug_report</i>
    </a>
    <a data-toggle="tooltip" data-placement="top" class="cache_clear" title="Clear Cache">
        <span class="material-icons" aria-hidden="true">cached</span>
    </a>

    @can('settings')
        <a data-toggle="tooltip" data-placement="top" href="{{ route('cms.settings') }}" title="">
            <i class="material-icons">build</i>
        </a>
    @endcan

    <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('admin.logout') }}">
        <i class="material-icons">power_settings_new</i>
    </a>
</div>

</div>
</div>

