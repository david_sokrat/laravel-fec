<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Employees
 * @package App
 */
class Employees extends Model
{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $guarded = [ ];

    /**
     * @return mixed
     */
    public function translations()
    {
        return $this->hasMany('App\Models\Content\EmployeeI18n', 'id', 'id');
    }
}
