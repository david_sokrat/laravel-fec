@extends('backend.layouts.backend')


@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">SEO {{tc('settings')}}</a></li>
                        <li class="active">Robots.txt</li>
                    </ol>




                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" >


                        <br><form class="form-horizontal" method="post" action="{{route('cms.robots.save')}}">

                            {{ csrf_field() }}
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <label for="scripts">Robots for .Ge Domain</label>
                                <textarea class="form-control" name="robotsge" id="scripts" rows="7">{{$robotsge}}</textarea>

                                <br>
                                <button type="submit" class="btn btn-primary">{{tc('save')}}</button>
                            </div>

                        </form>



                    </div>

                </div>


            </div>
        </div>
    </div>


@stop

