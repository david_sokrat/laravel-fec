<?php

return [
    'langs'          => [ 'ka', 'ru', 'en', 'tr' ],

    'locales' => [
        'ka' => 'KA',
        'ru' => 'RU',
        'en' => 'EN',
        'tr' => 'TR',
    ],

    'city' => [
        'ba' => 'Batumi',
        'tb' => 'Tbilisi'
    ]
];