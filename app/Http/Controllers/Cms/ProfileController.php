<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Admin;
use Session;
use Image;
use Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $admin = Admin::find(Session::get('admin_profile_id'));

        return view('cms.pages.profile.profile')->withAdmin($admin);
    }

    public function edit(Request $request)
    {
        $admin = Admin::findorfail(Session::get('admin_profile_id'));

        if(!empty($request->name)) {

            $admin->name = $request->name;
        }

        if(!empty($request->email)) {

            if($admin->email != $request->email) {

                $checkmail = Admin::where('email', $request->email)->first();

                if(empty($checkmail->email)) {

                    $admin->email = $request->email;
                } else {

                    Session::put('error', 'Profile edit failed!');

                    return redirect()->route('cms.profile');
                } 
            } else {

                $admin->email = $request->email;
            }
        }

        //bcrypt($request->newpassword);
        if(!empty($request->newpassword)) {

            $admin->password = Hash::make($request->newpassword);
        }

        if(!empty($request->image)) {
            $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = storage_path('app/public/img/' . $filename);
            Image::make($image->getRealPath())->save($location);
            $admin->avatar = $filename;
        }

        $admin->save();

        Session::put('success', 'Profile was successfully edited!');

        return redirect()->route('cms.profile');
    }
}
