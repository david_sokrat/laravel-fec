<section id="tax-dispute-promo">
    <script id="template2" type="notjs">
        <div class="scene2"></div>
    </script>
    <div id="content2"></div>
    <div class="wrapper">
        <div class="red-brilliant brl-item">
            <div class="brl-wrap">
                <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
            </div>
            <div class="rd-text-text-dispute">
                <h1>{{ $translations['academy.main'] }}</h1>
                <p>{{ $translations['academy.main_text'] }}</p>
            </div>
        </div>
    </div>
</section>