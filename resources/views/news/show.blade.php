@extends('main')

@section('title', '| Article')

@section('stylesheets')
    <link href="{{ asset('css/article.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    @include('partials.article._promo-block-article') 
    @include('partials.article._article-content') 
    @include('partials.article._article-btns')
@endsection           