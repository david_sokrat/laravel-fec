@extends('backend.layouts.backend')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">


                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>
                        <li><a href="#">{{tc('details')}}</a></li>
                        <li class="active">{{tc('contact.info')}} </li>
                    </ol>



                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" > <br>

                        <form action="{{route('cms.contacts.save')}}" method="post">

                            {{ csrf_field() }}



                            <div class="row"> <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="phone" value="{{$contacts->phone}}" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Phone">
                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                </div> </div>



                            <div class="row"> <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                                <input type="text" name="mail" value="{{$contacts->mail}}" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Default Mail">
                                <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                                </div> </div>


                            <div class="row"> <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                                    <input type="text" name="latitude" value="{{$contacts->latitude}}" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Default Mail">
                                    <span class="fa fa-compass form-control-feedback left" aria-hidden="true"></span>
                                </div> </div>

                            <div class="row"> <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                                    <input type="text"  name="longitude" value="{{$contacts->longitude}}" class="form-control has-feedback-left" id="inputSuccess2" placeholder="Default Mail">
                                    <span class="fa fa-compass form-control-feedback left" aria-hidden="true"></span>
                                </div> </div>


                         



                             <button href="#address" class="btn btn-info btn-sm" data-toggle="collapse"><i class="fa fa-language"></i> {{tc('addresses')}}</button>
                                <div id="address" class="collapse">
                            @foreach(getLocales() as $locales)
                            <div class="row">  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="address-{{$locales->locale}}" @foreach($address as $addr) @if($locales->locale == $addr->locale) value="{{$addr->value}}" @endif @endforeach class="form-control has-feedback-left" id="inputSuccess2" placeholder="Address - {{$locales->locale}}">
                                    <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                </div></div>
                            @endforeach
                            </div>





                            <div class="row">  <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <button type="submit" class="btn btn-success">{{tc('save')}}</button>
                            </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

