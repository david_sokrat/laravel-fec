<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('partials._head')
    </head>

    <body id="body">
        <div style="position: fixed !important; top: 20px !important;">
            @include('partials._messages')
        </div>
        
        <section id="request-email-modal" style="display: block;">
            <div class="request-email-modal-wrapper">
                <a href="{{ route('home') }}">
                    <div class="auth-icon-modal active">
                        <div class="auth-modal-close"></div>
                    </div>
                </a>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form id="request-email" method="POST" action="{{ route('admin.password.email') }}">
                    {{ csrf_field() }}

                    <h2>{{ $translations['user_email_password_drop_down_title'] }}</h2>
                    <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="user-email" placeholder="{{ $translations['reset_password'] }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="req-btn-wrap">
                        <button type="submit" class="req-email-btn"><span>{{ $translations['user_password_drop_down_btn'] }}</span></button>
                    </div>
                </form>
            </div>
        </section>

        @include('partials._javascript')

    </body>
</html>