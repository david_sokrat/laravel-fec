{{-- @extends('main')

@section('title', '| Edit article')

@section('stylesheets')
    <link href="{{ asset('css/tax-dispute.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    <section class="form-dispute-page">
        <div class="wrapper">
            {!! Form::model($new, ['route' => ['update-article', Session::get('locale')]]) !!}
                {{ csrf_field() }}

                {{ Form::hidden('slug', $new->slug, array()) }}
                
                {{ Form::label('title', 'Title:') }}
                {{ Form::text('title', null, ["class" => 'input-fields']) }}

                {{ Form::label('date', 'Date:') }}
                {{ Form::date('date', null, ["class" => 'input-fields']) }}

                {{ Form::label('slug', 'Slug: ', array()) }}
                {{ Form::text('slug', null, array('class' => 'input-fields', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}
        
                {{ Form::label('navigation_id', 'Year: ', array()) }}
                {{ Form::select('navigation_id', $navigation, null, ['class' => 'input-fields']) }}

                {{ Form::label('body', 'Content:') }}
                {{ Form::textarea('body', null, ["class" => 'input-fields']) }}

                {{ Form::label('image_name', 'Upload image: ') }}
                {{ Form::file('image_name', null, ["class" => 'input-fields']) }}

                <div class="back-to-news-link"></div>
        
                <div class="row-edit-btn">
                    {!! Html::linkRoute('news.show', 'Cancel', array(Session::get('locale'), $new->slug), array('class' => 'btn-delete')) !!}
                </div>
                <div class="row-edit-btn">
                    {{ Form::submit('Save changes', ['class' => 'btn-save']) }}
                </div>
            {!! Form::close() !!}
        </div>    
    </section>
@endsection --}}