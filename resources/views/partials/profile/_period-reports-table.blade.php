<section id="period-reports-table">
    <div class="profile-wrapper">
        <div id="adaptive-period-table-header" class="row table-item">
            <div id="adaptive-status-arrow-parent" class="row-item1">
                {{ $translations['cms_report_status'] }} <img id="adaptive-status-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
            </div>
            <div class="row-item2">
                {{ $translations['cms_report_naming'] }} <img id="adaptive-name-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
            </div>
            <div id="adaptive-load-arrow-parent" class="row-item3">
                {{ $translations['cms_report_download_date'] }} <img id="adaptive-load-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
            </div>
            <div id="adaptive-amount-arrow-parent" class="row-item4">
                {{ $translations['cms_report_number_downloads'] }} <img id="adaptive-amount-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
            </div>
            <div class="row-item5">
                {{ $translations['cms_report_donwload'] }} <img id="adaptive-download-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
            </div>
        </div>
        <div class="row-items">
            <div class="row table-item reports-table-header">
                <div id="status-arrow-parent" class="row-item1">
                    {{ $translations['cms_report_status'] }} <img id="status-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
                </div>
                <div class="row-item2">
                    {{ $translations['cms_report_naming'] }} <img id="name-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
                </div>
                <div id="load-arrow-parent" class="row-item3">
                    {{ $translations['cms_report_download_date'] }} <img id="load-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
                </div>
                <div id="amount-arrow-parent" class="row-item4">
                    {{ $translations['cms_report_number_downloads'] }} <img id="amount-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
                </div>
                <div class="row-item5">
                    {{ $translations['cms_report_donwload'] }} <img id="download-arrow" src="{{ asset('img/profile-table-filter-arrow.svg') }}">
                </div>
            </div>

            @foreach ($reports as $report)
                @if ($report->date == "сегодня")
                    <div class="row table-item filterable-element" 
                        data-downloads="{{ $report->number_of_downloads }}" 
                        data-status="0" 
                        data-date-of-download="0000-00-00"
                    >
                @elseif ($report->date == "вчера")
                    <div class="row table-item filterable-element" 
                        data-downloads="{{ $report->number_of_downloads }}" 
                        data-status="0" 
                        data-date-of-download="0000-00-01"
                    >
                @else
                    <div class="row table-item filterable-element" 
                        data-downloads="{{ $report->number_of_downloads }}" 
                        data-status="0" 
                        data-date-of-download="{{ $report->date }}"
                    >
                @endif

                    @if($report->status == false) 
                        <div class="row-item1">
                            <svg version="1.1" class="in-progress" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                                <g>
                                    <circle fill-rule="evenodd" clip-rule="evenodd" fill="#FFDE00" cx="5" cy="5" r="5"/>
                                </g>
                            </svg>
                        </div>
                    @endif

                    @if($report->status == true)
                        <div class="row-item1">
                            <svg version="1.1" class="complete" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                                <g>
                                    <circle fill-rule="evenodd" clip-rule="evenodd" fill="#20D11C" cx="5" cy="5" r="5"/>
                                </g>
                            </svg>
                        </div>
                    @endif

                    <div class="row-item2">{{ $translations_contents[$report->title] }}</div>
                    <div class="row-item3">
                        @if ($report->date == "сегодня")
                            {{ $translations['date_today'] }}
                        @elseif ($report->date == "вчера")
                            {{ $translations['date_yesterday'] }}
                        @else
                            {{ $report->date }}
                        @endif
                    </div>
                    <div class="row-item4">{{ $report->number_of_downloads }}</div>
                    <div class="row-item5">
                        <form id="file-downloads-{{ $report->id }}" class="table-docs-download-icons" action="{{ URL::to('reports/store') }}" method="POST" data-number-of-downloads="{{ $report->number_of_downloads }}">
                            {{ csrf_field() }}

                            <div class="download-file">
                                <a class="file-download-link" download href="{{ asset('storage/' . $report->path_to_excel) }}">
                                    <img class="docs-for-download" src="{{ asset('img/excel.svg') }}">
                                </a>
                            </div>
                            <div class="download-file">
                                <a class="file-download-link" download href="{{ asset('storage/' . $report->path_to_docs) }}">
                                    <img class="docs-for-download" src="{{ asset('img/docs.svg') }}">
                                </a>
                            </div>
                            <div class="download-file">
                                <a class="file-download-link" download href="{{ asset('storage/' . $report->path_to_pdf) }}">
                                    <img class="docs-for-download" src="{{ asset('img/pdf.svg') }}">
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>