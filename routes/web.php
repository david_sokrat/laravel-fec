<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('language/change', 'LanguageController@postChangeLanguage')->name('language.change');

Route::group([
    'prefix' => '{locale?}',
    'middleware' => ['setlocale'],
    'where' => ['locale' => implode('|', array_keys(config('translatable.locales')))]], 
    function() {
        Route::resource('navigation', 'NavigationController');
        Route::post('set-contacts', 'ContactsController@changeContacts')->name('set-contacts');

        //Managing news(create, edit, show, destroy)
        Route::resource('news', 'NewsController', ['only' => ['index']]);
        Route::get('news/index', 'NewsController@index')->name('articles');
        Route::get('news/{slug}', ['as' => 'show.new', 'uses' => 'NewsController@show'])->where('slug', '[\w\d\-\_]+');
        Route::get('news/show', 'NewsController@show');

        //Managing profile reports(create, update, edit, destroy)
        Route::resource('reports', 'ReportController');
        Route::get('reports', 'ReportController@index')->name('reports');
        
        //Managing pages routes
        Route::get('academy', 'PagesController@getAcademy')->name('academy');
        Route::get('audit', 'PagesController@getAudit')->name('audit');
        Route::get('company', 'PagesController@getCompany')->name('company');
        Route::get('contacts', 'PagesController@getContacts')->name('contacts');
        Route::get('finance', 'PagesController@getFinance')->name('finance');
        Route::get('legal', 'PagesController@getLegal')->name('legal');
        Route::get('tax-dispute', 'PagesController@getTaxDispute')->name('tax-dispute');
        Route::post('audit-contact', 'PagesController@postAuditForm')->name('audit-form');
        Route::post('acount-contact', 'PagesController@postAcountForm')->name('acount-form');
        Route::post('academy-contact', 'PagesController@postAcademyForm')->name('academy-form');
        Route::post('academy-contact', 'PagesController@postAuditPageForm')->name('audit-page-form');
        Route::get('/', 'PagesController@getIndex')->name('home');

        // Authorization routes
        Auth::routes();
        Route::get('logout', 'Auth\LoginController@userLogout')->name('logout');
    }
);

//Cms routes and admin auth routes
Route::prefix('admin')->group(function() {
    //Admin auth routes
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

    //Admin reset password routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset'); 
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

});

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::prefix('cms')->group(function() {
    //Cms news routes
    Route::get('pages/news/news', 'Cms\NewsController@index')->name('news.index');
    Route::get('pages/news/create', 'Cms\NewsController@create')->name('news.create');
    Route::post('pages/news', 'Cms\NewsController@store')->name('news.store');
    Route::get('pages/{news}/news', 'Cms\NewsController@edit')->name('news.edit');
    Route::put('pages/{news}', 'Cms\NewsController@update')->name('news.update');
    Route::get('pages/{news}', 'Cms\NewsController@destroy')->name('news.destroy');

    //Cms reports routes
    Route::get('pages/reports/reports', 'Cms\ReportController@index')->name('reports.index');
    Route::get('pages/reports/create', 'Cms\ReportController@create')->name('reports.create');
    Route::post('pages/reports', 'Cms\ReportController@store')->name('reports.store');
    Route::get('pages/{reports}/reports', 'Cms\ReportController@edit')->name('reports.edit');
    Route::put('pages/{reports}', 'Cms\ReportController@update')->name('reports.update');
    Route::get('pages/{reports}', 'Cms\ReportController@destroy')->name('reports.destroy');

    //Mainpage route
    Route::get('pages/mainpage/mainpage', 'Cms\MainpageController@index')->name('cms.dashboard');

    //Cms change laguage route
    Route::post('language/change', 'LanguageController@cmsChangeLanguage')->name('cms.language.change');

    //Profile routes
    Route::get('profile', 'Cms\ProfileController@index')->name('cms.profile');
    Route::post('profile/edit', 'Cms\ProfileController@edit')->name('cms.profile.edit');
});