<footer>
    <div class="pull-right">
        &copy; ITERS AGENCY
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<!-- jQuery -->
<script src="{{ asset('back/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('js/cms.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('back/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('back/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('back/vendors/nprogress/nprogress.js') }}"></script>
<!-- jQuery custom content scroller -->
<script src="{{ asset('back/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
{{--<script src="{{asset('back/vendors/switchery/dist/switchery.min.js')}}"></script>--}}
<!-- jQuery Tags Input -->
<script src="{{ asset('back/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<!-- Custom Theme Scripts -->
<script src="{{ asset('back/select2.min.js') }}"></script>
<script src="{{ asset('back/ckeditor/ckeditor/ckeditor.js') }}"> </script>
<script src="{{ asset('back/build/js/custom.min.js') }}"></script>

@yield('scripts')

</body>
</html>