<header id="header" class="profile-header-visible">
    <div class="wrapper-header">
        <div class="logo">
            <a href="{{ route('home', ['lang' => Session::get('locale')]) }}">
                <img src="{{ asset('img/fec_logo.svg') }}" alt="FEC logo" />
            </a>
        </div>
        <div class="align-header profile-adaptive-header">
            <h2>{{ $translations['header_nav_profile'] }}</h2>
            <div class="profile-info-items">
                <div class="profile-info-item item-top">
                    <span class="prof-item-data">{{ $user->name }}</span>
                </div>
                <div class="profile-info-item item-middle">
                    <span class="prof-item-header">{{ $translations['profile_company_inn'] }}</span>
                    <span class="prof-item-data">{{ $user->inn }}</span>
                </div>
                <div class="profile-info-item item-bottom">
                    <span class="prof-item-header">{{ $translations['profile_company_type_title'] }}</span>
                    <span class="prof-item-data">{{ $translations['profile_company_type'] }}</span>
                </div>
            </div>
        </div>
        <div class="client-profile">
            @if(Auth::check())
                <a href="{{ route('logout', Session::get('locale')) }}"><img src="{{ asset('img/logout.svg') }}">{{ $translations['header_nav_logout'] }}</a>
            @endif
            @if(Auth::guest())
                <a href="#"><img src="{{ asset('img/user-shape.svg') }}">{{ $translations['header_nav_profile'] }}</a>
            @endif
        </div>
    </div>
</header>