@extends('main')

@section('title', '| Profile')

@section('stylesheets')
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    @if(Auth::check())
    
        @include('partials.profile._header') 
        @include('partials.profile._profile-status') 
        @include('partials.profile._period-reports-table') 
        @include('partials.profile._profile-table-records-paginator') 

    @endif
@endsection