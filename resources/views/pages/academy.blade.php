@extends('main')

@section('title', '| Academy')

@section('stylesheets')
    <link href="{{ asset('css/academy.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = true;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('canvas')
    <canvas id="canvas2"></canvas>
@endsection

@section('content')
    @include('partials.academy._tax-dispute-promo') 
    @include('partials.academy._tax-dispute-text-content') 
    @include('partials.academy._tax-order-form') 
@endsection