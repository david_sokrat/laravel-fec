<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        @include('partials._head')
    </head>

    <body id="body">
        @if (Session::get('city') === null && empty(Session::get('city')))
            {{ Session::put('city', 'batumi') }}
        @endif

        <div style="position: fixed !important; top: 20px !important;">
            @include('partials._messages')
        </div>

        @yield('canvas')

        @include('partials._burger-menu')
        @include('partials._auth-modal')
        @include('partials._burger-menu-modal')
        @include('partials._sub-header')       
        @include('partials._header')
        
        @yield('content')

        @include('partials._footer')
        @include('partials._javascript')

        @yield('google-maps')
    </body>

</html>