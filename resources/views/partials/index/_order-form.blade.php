<section id="order-form">

    @include('partials._messages')

    <div class="wrapper">
        <form id="audit-form" class="tabcontent" action="{{ route('audit-form', Session::get('locale')) }}" method="POST">
            {{ csrf_field() }}
            <label for="audit-form">{{ $translations['audit_form_label'] }}</label>
            <div class="input-fields">
                <input type="hidden" name="form_name" value="Audit main page form">
                <input type="text" placeholder="{{ $translations['audit_form_company'] }}" name="company" class="company-name">
                <input type="text" placeholder="{{ $translations['audit_form_phone'] }}" name="phone" class="company-phone">
                <input type="text" placeholder="{{ $translations['audit_form_company_type'] }}" name="type_of_maintanance" class="company-type-of-maintanance">
                <input type="text" placeholder="{{ $translations['audit_form_company_adress'] }}" name="address" class="company-adress">
                <input type="text" placeholder="{{ $translations['audit_form_company_email'] }}" name="email" class="company-email">
                <input type="text" placeholder="{{ $translations['audit_form_company_programm'] }}" name="pc_programm" class="company-pc-programm">
                <textarea name="client_message" class="client-message" placeholder="{{ $translations['audit_form_message'] }}"></textarea>
            </div>
            <div class="btn-wrap">
                <button type="submit" form="audit-form"><span>{{ $translations['audit_form_submit_btn'] }}</span></button>
            </div>
        </form>
        <form id="finance-form" class="tabcontent" action="{{ route('acount-form', Session::get('locale')) }}" method="POST">
            {{ csrf_field() }}
            <label for="finance-form">{{ $translations['audit_form_label_acounting'] }}</label>
            <div class="input-fields">
                <input type="hidden" name="form_name" value="Acount form">
                <input type="text" placeholder="{{ $translations['audit_form_company'] }}" name="acount_company" class="company-name">
                <input type="text" placeholder="{{ $translations['audit_form_phone'] }}" name="acount_phone" class="company-phone">
                <input type="text" placeholder="{{ $translations['audit_form_company_type'] }}" name="acount_type-of-maintanance" class="company-type-of-maintanance">
                <input type="text" placeholder="{{ $translations['audit_form_company_adress'] }}" name="acount_adress" class="company-adress">
                <input type="text" placeholder="{{ $translations['audit_form_company_email'] }}" name="acount_email" class="company-email">
                <input type="text" placeholder="{{ $translations['audit_form_company_programm'] }}" name="acount_pc-programm" class="company-pc-programm">
                <textarea name="client_message" class="client-message" placeholder="{{ $translations['audit_form_message'] }}"></textarea>
            </div>
            <div class="btn-wrap">
                <button type="submit" form="finance-form"><span>{{ $translations['audit_form_submit_btn'] }}</span></button>
            </div>
        </form>
        <form id="guide-form" class="tabcontent">
            <div class="select-inputs">
                <h3 class="guide-from-header">{{ $translations['filter_guide_header'] }}</h3>
                <div class="select-input">
                    <label for="business">{{ $translations['filter_guide_label'] }}</label>
                    <select id="business" name="business">
                        <option data-binary="false" value="Choose">{{ $translations['filter_guide_choose'] }}</option>
                        <option data-binary="3333" value="Commertial non(PIE)">{{ $translations['filter_guide_commrece'] }}</option>
                        <option data-binary="3331" value="Bank">{{ $translations['filter_guide_bank'] }}</option>
                        <option data-binary="3331" value="Insurance Company">{{ $translations['filter_guide_insurance'] }}</option>
                        <option data-binary="3331" value="Microfinancing Organisation">{{ $translations['filter_guide_microfinance'] }}</option>
                        <option data-binary="3331" value="Entities definded by goverment as PIE">{{ $translations['filter_guide_goverment'] }}</option>
                        <option data-binary="3331" value="Investment Fund">{{ $translations['filter_guide_fund'] }}</option>
                        <option data-binary="3331" value="Credit Union">{{ $translations['filter_guide_credit'] }}</option>
                        <option data-binary="3331" value="Pension Fund">{{ $translations['filter_guide_pension'] }}</option>
                        <option data-binary="3332" value="NGO">{{ $translations['filter_guide_ngo'] }}</option>
                    </select>
                </div>
                <div class="select-input">
                    <label for="revenue">{{ $translations['filter_guide_revenue'] }}</label>
                    <select id="revenue" name="revenue">
                        <option data-binary="false" value="Choose">{{ $translations['filter_guide_choose'] }}</option>
                        <option data-binary="0001" value="> 100 Million GEL">> 100 Million GEL</option>
                        <option data-binary="0010" value="20-100 Million GEL">20-100 Million GEL</option>
                        <option data-binary="0100" value="2-20 Million GEL">2-20 Million GEL</option>
                        <option data-binary="1000" value="0-2 Million GEL">0-2 Million GEL</option>
                    </select>
                </div>
                <div class="select-input">
                    <label for="assets">{{ $translations['offer_bussiness_guide'] }}</label>
                    <select id="assets" name="assets">
                        <option data-binary="false" value="Choose">{{ $translations['filter_guide_choose'] }}</option>
                        <option data-binary="0001" value="> 50 Million GEL">> 50 Million GEL</option>
                        <option data-binary="0010" value="10-50 Million GEL">10-50 Million GEL</option>
                        <option data-binary="0100" value="1-10 Million GEL">1-10 Million GEL</option>
                        <option data-binary="1000" value="0-1 Million GEL">0-1 Million GEL</option>
                    </select>
                </div>
                <div class="select-input">
                    <label for="average">{{ $translations['filter_guide_average'] }}</label>
                    <select id="average" name="average">
                        <option data-binary="false" value="Choose">{{ $translations['filter_guide_choose'] }}</option>
                        <option data-binary="0001" value="> 250">> 250</option>
                        <option data-binary="0010" value="50-250">50-250</option>
                        <option data-binary="0100" value="10-50">10-50</option>
                        <option data-binary="1000" value="0-10">0-10</option>
                    </select>
                </div>
            </div>
            <h2>{{ $translations['filter_guide_results'] }}</h2>
            <p>{{ $translations['filter_guide_results_descr'] }} <a href="#">See the new law full text</a></p>
            <div class="result-table-wrapper">
                <div class="result-table">
                    <div id="result-table-header" class="row table-item">
                        <div class="row-item1"><span>{{ $translations['filter_guide_category'] }}</span></div>
                        <div class="row-item2"><span>{{ $translations['filter_guide_framework'] }}</span></div>
                        <div class="row-item3"><span>{{ $translations['filter_guide_preparing'] }}</span></div>
                        <div class="row-item4"><span>{{ $translations['filter_guide_presenting'] }}</span></div>
                        <div class="row-item5"><span>{{ $translations['filter_guide_publishing'] }}</span></div>
                        <div class="row-item6"><span>{{ $translations['filter_guide_audit'] }}</span></div>
                    </div>
                    <div id="category-0" class="row table-item">
                        <div class="row-item1">{{ $translations['filter_guide_pie'] }}</div>
                        <div class="row-item2">{{ $translations['filter_guide_ifrs'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                    </div>
                    <div id="category-1" class="row table-item">
                        <div class="row-item1">I</div>
                        <div class="row-item2">{{ $translations['filter_guide_ifrs'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                    </div>
                    <div id="category-2" class="row table-item">
                        <div class="row-item1">II</div>
                        <div class="row-item2">{{ $translations['filter_guide_sme'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                    </div>
                    <div id="category-3" class="row table-item">
                        <div class="row-item1">III</div>
                        <div class="row-item2">{{ $translations['filter_guide_sme'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                    </div>
                    <div id="category-4" class="row table-item">
                        <div class="row-item1">IV</div>
                        <div class="row-item2">{{ $translations['filter_guide_special'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                    </div>
                    <div id="category-5" class="row table-item">
                        <div class="row-item1">{{ $translations['filter_guide_ngo'] }}</div>
                        <div class="row-item2">{{ $translations['filter_guide_special'] }}</div>
                        <div class="row-item3"><img class="ok-icon" src="{{ asset('img/ok-icon.svg') }}" /></div>
                        <div class="row-item4"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                        <div class="row-item5"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                        <div class="row-item6"><img class="not-available-icon" src="{{ asset('img/not-available-icon.svg') }}" /></div>
                    </div>
                </div>
                <div class="adaptive-header adaptive-row-item1"><span>{{ $translations['filter_guide_category'] }}</span></div>
                <div class="adaptive-header adaptive-row-item2"><span>{{ $translations['filter_guide_framework'] }}</span></div>
                <div class="adaptive-header adaptive-row-item3"><span>{{ $translations['filter_guide_preparing'] }}</span></div>
                <div class="adaptive-header adaptive-row-item4"><span>{{ $translations['filter_guide_presenting'] }}</span></div>
                <div class="adaptive-header adaptive-row-item5"><span>{{ $translations['filter_guide_publishing'] }}</span></div>
                <div class="adaptive-header adaptive-row-item6"><span>{{ $translations['filter_guide_audit'] }}</span></div>
            </div>
            {{-- <p>{{ $translations['filter_guide_bottom_descr'] }}</p> --}}
        </form>
    </div>
</section>