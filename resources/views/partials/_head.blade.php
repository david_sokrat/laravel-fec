<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>FEC @yield('title')</title>

<link href="{{ asset('css/normalize.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/reset.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/parsley.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css"/>

@yield('stylesheets')

<link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/owl.theme.default.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ mix('css/media.css') }}" rel="stylesheet" type="text/css"/>

@yield('scripts')