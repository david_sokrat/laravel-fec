<section id="tax-dispute-promo">
    <script id="template1" type="notjs">
        <div class="scene1"></div>
    </script>
    <div id="content1"></div>
    <div class="wrapper">
        <div class="red-brilliant brl-item">
            <div class="brl-wrap">
                <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
            </div>
            <div class="rd-text-text-dispute">
                <h1>{{ $translations['legal.main'] }}</h1>
                <p>{{ $translations['legal.main_text'] }}</p>
            </div>
        </div>
    </div>
</section>