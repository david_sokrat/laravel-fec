<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Article;
use App\Navigation;
use App\TranslationContent;
use App\Locales;
use Session;
use Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Article::orderBy('id', 'desc')->paginate(6);
        $trans = TranslationContent::all();

        return view('news/index')->withNews($news)->withTrans($trans);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $slug)
    {
        $translations_contents = DB::table('translations_contents')->where('slug', $slug)->first();
        $new = Article::find($translations_contents->article_id);

        return view('news/show')->withNew($new);
    }
}
