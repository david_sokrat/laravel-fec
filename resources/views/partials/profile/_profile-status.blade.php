<section id="profile-status">
    <div class="profile-wrapper">
        <div class="prof-status-flex">
            <div class="prof-status-header">
                <h2>{{ $translations['cms_sidebar_nav_reports'] }}</h2>
                <span class="status-data">{{ count($reports) }}</span>
            </div>
            <div class="prof-status-label">
                <span>
                    <svg version="1.1" id="in-progress" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                        <g>
                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="#FFDE00" cx="5" cy="5" r="5"/>
                        </g>
                    </svg>
                    {{ $translations['cms_report_progress'] }}
                </span>
            </div>
            <div class="prof-status-label">
                <span>
                    <svg version="1.1" class="complete" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve">
                        <g>
                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="#20D11C" cx="5" cy="5" r="5"/>
                        </g>
                    </svg>
                    {{ $translations['cms_report_complete'] }}
                </span>
            </div>
        </div>
    </div>
</section>