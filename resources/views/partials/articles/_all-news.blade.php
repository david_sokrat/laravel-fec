<section id="all-news">
    <div class="wrapper">
        <div class="owl-carousel owl-theme owl-loaded">
            <div class="owl-stage-outer owl-news-page">
                <div class="filter-news news-block owl-stage">
                    
                    @foreach ($news as $new)
                        
                        <div class="year{{ substr($new->date, 0, 4) }} news-item owl-item">
                            <div id="item1" class="item" style="background: url( {{ asset('storage/public/img/' . $new->image_name) }}) center top no-repeat; background-size: cover;"></div>
                            <div class="news-descr">
                                <a href="{{ route('show.new', ['lang' => Session::get('locale'), 'slug' => $translations_contents[$new->slug]]) }}"><span class="text-link">{{ $translations_contents[$new->title] }}<span></a>
                                <span class="date-of-news">{{ $new->date }}</span>
                            </div>
                        </div>
                        
                    @endforeach                                            
                    
                </div>
            </div>
        </div>
    </div>
</section>