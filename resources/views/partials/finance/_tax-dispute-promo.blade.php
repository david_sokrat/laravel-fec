<section id="tax-dispute-promo">
    <script id="template0" type="notjs">
        <div class="scene0"></div>
    </script>
    <div id="content0"></div>
    <div class="wrapper">
        <div class="red-brilliant brl-item">
            <div class="brl-wrap">
                <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
            </div>
            <div class="rd-text-text-dispute">
                <h1>{{ $translations['finance.main'] }}</h1>
                <p>{{ $translations['finance.main_text'] }}</p>
            </div>
        </div>
    </div>
</section>