@extends('main')

@section('title', '| Password reset')

@section('scripts')
    <script type="text/javascript">
        mainStatus = true;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

<section id="reset-password-modal">
    <div class="reset-password-wrapper">
        <a href="{{ route('home') }}">
            <div class="auth-icon-modal active">
                <div class="auth-modal-close"></div>
            </div>
        </a>

        <form id="reset-password-form" method="POST" action="{{ route('password.request', Session::get('locale')) }}">
            {{ csrf_field() }}

            <h2>{{ $translations['user_email_password_drop_down_title'] }}</h2>
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="input-fields{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="user-email" placeholder="{{ $translations['audit_form_company_email'] }}" name="email" value="{{ $email or old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-fields{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="user-password" placeholder="{{ $translations['reset_password'] }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-fields{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="user-password" placeholder="{{ $translations['confirm_reset_password'] }}" name="password_confirmation" required>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="req-btn-wrap">
                <button type="submit" class="req-email-btn"><span>{{ $translations['confirm_reset_password_btn'] }}</span></button>
            </div>
        </form>
    </div>
</section>

@section('content')
    @include('partials.index._promo-block')
    @include('partials.index._brilliant-services')
    @include('partials.index._company-achivments')
    @include('partials.index._company-offers')
    @include('partials.index._order-form')
    @include('partials.index._useful-docs')
    @include('partials.index._partners')
@endsection