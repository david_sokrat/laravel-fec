{{-- @extends('main')

@section('title', '| Create report')

@section('stylesheets')
    <link href="{{ asset('css/tax-dispute.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    <section id="tax-order-form" class="form-dispute-page">
        <div class="wrapper">
            <h2>Создайте новость</h2>
            {!! Form::open(['route' => ['store-report', Session::get('locale')], 'enctype' =>"multipart/form-data", 'files' => true, 'id' => 'tax-dispute-form', 'data-parsley-validate' => '', 'method' => 'PUT']) !!}
                {{ Form::label('status', 'Статус:', array()) }}
                {{ Form::radio('status', false, array()) }} В процессе
                {{ Form::radio('status', true, array()) }} Выполнено
                
                {{ Form::label('title', 'Заголовок: ', array()) }}
                {{ Form::text('title', null, array('class' => 'input-fields', 'maxlength' => '250', 'required' => '')) }}
            
                {{ Form::label('date', 'Дата репорта: ', array()) }}
                {{ Form::date('date', null, array('class' => 'input-fields', 'required' => '')) }}

                {{ Form::label('path_to_excel', 'Загрузить файл: ', array()) }}
                {{ Form::file('path_to_excel', null, array('class' => 'btn-save', 'required' => '')) }}

                {{ Form::label('path_to_docs', 'Загрузить файл: ', array()) }}
                {{ Form::file('path_to_docs', null, array('class' => 'btn-save', 'required' => '')) }}

                {{ Form::label('path_to_pdf', 'Загрузить файл: ', array()) }}
                {{ Form::file('path_to_pdf', null, array('class' => 'btn-save', 'required' => '')) }}

                {{ Form::submit('СОЗДАТЬ РЕПОРТ', array('class' => 'btn-create')) }}
            {!! Form::close() !!}
        </div>
    </section>
@endsection --}}