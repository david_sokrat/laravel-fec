@extends('cms.layouts.backend')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('news.index') }}">{{ $translations['cms_sidebar_nav_title_news'] }}</a></li>
                        <li class="active">{{ $translations['cms_sidebar_nav_editing'] }}</li>
                    </ol>

                    @include('partials._messages')

                    <div class="x_content"><br>
                        {!! Form::model($new, ['route' => ['news.update', $new->id], 'files' => true, 'class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '', 'method' => 'PUT']) !!}
                            {{ csrf_field() }}
                            
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    @php $activator = 0; @endphp
                                    @foreach($locales as $loc)
                                        <li role="presentation" @if($activator == 0) class="active" @else class="" @endif>
                                            <a href="#{{ $loc->locale }}" data-toggle="tab" aria-expanded="true">
                                                @if(Session::get('locale') == 'ru')
                                                    {{ $loc->name_ru }}
                                                @elseif(Session::get('locale') == 'en')
                                                    {{ $loc->name_en }}
                                                @elseif(Session::get('locale') == 'ka')
                                                    {{ $loc->name_ka }}
                                                @elseif(Session::get('locale') == 'tr')
                                                    {{ $loc->name_tr }}
                                                @endif
                                            </a>
                                        </li>
                                        @php $activator++ @endphp
                                    @endforeach
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    @php $activator = 0; @endphp
                                    @foreach($locales as $loc)
                                        <div role="tabpanel" class="tab-pane fade @if($activator == 0) active in  @endif" id="{{ $loc->locale }}">
                                            <div class="form-group">
                                                {{ Form::label('title' . $loc->locale, $translations['cms_item_title'], array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    {{ Form::text('title' . $loc->locale, $title[$activator], array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'firstText_' . $loc->locale, 'maxlength' => '250', 'required' => '')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('body' . $loc->locale, $translations['cms_tab_content'], array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    {{ Form::textarea('body' . $loc->locale, $body[$activator], array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'ckeditor_' . $loc->locale, 'required' => '')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('slug' . $loc->locale, $translations['cms_item_slug'], array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    {{ Form::text('slug' . $loc->locale, $slug[$activator], array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'id' => 'slug', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}
                                                </div>
                                            </div>
                                        </div>

                                        @php $activator++ @endphp
                                    @endforeach
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                {{ Form::label('date', $translations['cms_item_date'], array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    {{ Form::date('date', null, array('class' => 'form-control', 'style' => 'border-radius: 3px;', 'required' => '')) }}
                                </div>
                            </div>
                            <div class="form-group"> 
                                {{ Form::label('image_name', $translations['cms_tab_picture'], array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                                <div class="col-md-8 col-sm-8 col-xs-12"> 
                                    {{ Form::file('image_name', array('class' => 'form-control col-md-7 col-xs-12', 'style' => 'border-radius: 3px;', 'accept' => 'image/x-png,image/gif,image/jpeg', 'required' => '')) }}
                                    <img src="{{ asset('storage/public/img/' . $new->image_name) }}"  width="90" height="50">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2">
                                    {!! Html::linkRoute('news.index', 'cancel', null, array('class' => 'material-icons btn btn-default', 'style' => 'color: #d30004;', 'title' => 'Cancel')) !!}
                                    {!! Form::submit('check', ['class' => 'material-icons btn btn-default', 'style' => 'color: #6b9973;', 'title' => 'Save changes']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        @foreach($locales as $loc)
            CKEDITOR.replace( 'ckeditor_{{$loc->locale}}' );
        @endforeach
    </script>
    <script type="text/javascript">
        @foreach($locales as $loc)
            ft_{{$loc->locale}}=$('input#firstText_{{$loc->locale}}');sl_{{$loc->locale}}=$('input#slug_{{$loc->locale}}');
            ft_{{$loc->locale}}.keyup(function c(){sl_{{$loc->locale}}.val(slugIt(ft_{{$loc->locale}}.val()))});
        @endforeach
    </script>
    <script>
        //ეს სადმე JS ფაილში გაუშვი თუ გინდა
        function slugIt(e){return e=e.replace(/[^a-zA-Z ა-ჰ а-яА-Я 0-9]+/g, ''),e=e.replace(/ /g,"-"),e=e.replace(/ა/g,"a"),e=e.replace(/ბ/g,"b"),e=e.replace(/გ/g,"g"),e=e.replace(/დ/g,"d"),e=e.replace(/ე/g,"e"),e=e.replace(/ვ/g,"v"),e=e.replace(/ზ/g,"z"),e=e.replace(/თ/g,"t"),e=e.replace(/ი/g,"i"),e=e.replace(/კ/g,"k"),e=e.replace(/ლ/g,"l"),e=e.replace(/მ/g,"m"),e=e.replace(/ნ/g,"n"),e=e.replace(/ო/g,"o"),e=e.replace(/პ/g,"p"),e=e.replace(/ჟ/g,"j"),e=e.replace(/რ/g,"r"),e=e.replace(/ს/g,"s"),e=e.replace(/ტ/g,"t"),e=e.replace(/უ/g,"u"),e=e.replace(/ფ/g,"f"),e=e.replace(/ქ/g,"q"),e=e.replace(/ღ/g,"g"),e=e.replace(/ყ/g,"k"),e=e.replace(/შ/g,"sh"),e=e.replace(/ჩ/g,"ch"),e=e.replace(/ც/g,"c"),e=e.replace(/ძ/g,"dz"),e=e.replace(/წ/g,"w"),e=e.replace(/ჭ/g,"ch"),e=e.replace(/ხ/g,"x"),e=e.replace(/ჯ/g,"j"),e=e.replace(/ჰ/g,"h"),e=e.replace(/а/gi,"a"),e=e.replace(/б/gi,"b"),e=e.replace(/в/gi,"v"),e=e.replace(/г/gi,"g"),e=e.replace(/д/gi,"d"),e=e.replace(/е/gi,"e"),e=e.replace(/ё/gi,"jo"),e=e.replace(/ж/gi,"j"),e=e.replace(/з/gi,"z"),e=e.replace(/и/gi,"i"),e=e.replace(/й/gi,"j"),e=e.replace(/к/gi,"k"),e=e.replace(/л/gi,"l"),e=e.replace(/м/gi,"m"),e=e.replace(/н/gi,"n"),e=e.replace(/о/gi,"o"),e=e.replace(/п/gi,"p"),e=e.replace(/р/gi,"r"),e=e.replace(/с/gi,"s"),e=e.replace(/т/gi,"t"),e=e.replace(/у/gi,"u"),e=e.replace(/ф/gi,"f"),e=e.replace(/х/gi,"x"),e=e.replace(/ц/gi,"c"),e=e.replace(/ч/gi,"ch"),e=e.replace(/ш/gi,"sh"),e=e.replace(/щ/gi,"shch"),e=e.replace(/ъ/gi,""),e=e.replace(/ы/gi,"i"),e=e.replace(/ь/gi,""),e=e.replace(/э/gi,"ie"),e=e.replace(/ю/gi,"iu"),e=e.replace(/я/gi,"ya"),e}
    </script>
@endsection