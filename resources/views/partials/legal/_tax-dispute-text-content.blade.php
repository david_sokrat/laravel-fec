<section id="tax-dispute-text-content">
    <div class="wrapper">
        <!-- <p>{{ $translations['legal.content1'] }}</p> -->
        <div class="text-content-items">
<!--             <div class="tax-dispute-quota text-content-item">
                <img id="quotes" src="{{ asset('img/quotes.svg') }}" />
                <p>{{ $translations['legal.content2'] }}</p>
            </div> -->
            <div class="disputes-descr text-content-item">
                <h2>{{ $translations['legal.content_header'] }}</h2>
                <p>{!! $translations['legal.content3'] !!}</p>
<!--                 <p>{{ $translations['legal.content4'] }}</p>
                <p>{{ $translations['legal.content5'] }}</a></p> -->
            </div>
            <div class="disputes-tax-objects text-content-item">
                <h2>{{ $translations['legal.content_header2'] }}</h2>
                <p>{!! $translations['legal.content6'] !!}</p>
<!--                 <ul>
                    <li>{{ $translations['legal.content_li1'] }}</li>
                    <li>{{ $translations['legal.content_li2'] }}</li>
                    <li>{{ $translations['legal.content_li3'] }}</li>
                    <li>{{ $translations['legal.content_li4'] }}</li>
                    <li>{{ $translations['legal.content_li5'] }}</li>
                    <li>{{ $translations['legal.content_li6'] }}</li>
                </ul>
                <p>{{ $translations['legal.content7'] }}</p> -->
            </div>
<!--             <div class="calculation-tax-disputes text-content-item">
                <h2>{{ $translations['legal.content_header3'] }}</h2>
                <p>{{ $translations['legal.content8'] }}</p>
                <div class="dispute-table-wrap">
                    <div class="dispute-table">
                        <div class="header-layer"></div>
                        <div class="dispute-table-item">
                            <div class="dispute-debitor dispute-header1">{{ $translations['legal.content_table_deb'] }}</div>
                            <div class="dispute-debitor dispute-data1">60</div>
                            <div class="dispute-debitor dispute-data1">91</div>
                            <div class="dispute-debitor dispute-data1">62</div>
                            <div class="dispute-debitor dispute-data1">55</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-creditor dispute-header2">{{ $translations['legal.content_table_creditor'] }}</div>
                            <div class="dispute-creditor dispute-data2">90</div>
                            <div class="dispute-creditor dispute-data2">68</div>
                            <div class="dispute-creditor dispute-data2">91</div>
                            <div class="dispute-creditor dispute-data2">44</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-amount dispute-header3">{{ $translations['legal.content_table_amount'] }}</div>
                            <div class="dispute-amount dispute-data3">230000</div>
                            <div class="dispute-amount dispute-data3">197000</div>
                            <div class="dispute-amount dispute-data3">8500</div>
                            <div class="dispute-amount dispute-data3">670</div>
                        </div>
                        <div class="dispute-table-item">
                            <div class="dispute-type-of-record dispute-header4">{{ $translations['legal.content_table_record'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['legal.content_table_profit'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['legal.content_table_deb_amount'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['legal.content_table_deb_refund'] }}</div>
                            <div class="dispute-type-of-record dispute-data4">{{ $translations['legal.content_table_new_creditor'] }}</div>
                        </div>
                    </div>
                    <div class="adaptive-header adaptive-header-debit">{{ $translations['legal.content_table_deb'] }}</div>
                    <div class="adaptive-header adaptive-header-credit">{{ $translations['legal.content_table_creditor'] }}</div>
                    <div class="adaptive-header adaptive-header-amount">{{ $translations['legal.content_table_amount'] }}</div>
                    <div class="adaptive-header adaptive-header-record">{{ $translations['legal.content_table_record'] }}</div>
                </div>
            </div> -->
        </div>
    </div>
</section>