<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Article;
use App\Report;
use Session;
use Mail;
use App\Employees;
use App\EmployeesI18n;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller 
{

    public function getIndex() {
        $news = Article::orderBy('created_at', 'desc')->limit(6)->get();

        return view('pages.index')->withNews($news);
    }

    public function getTaxDispute() {
        return view('pages.tax-dispute');
    }

    public function getAcademy() {
        return view('pages.academy');
    }

    public function getAudit() {
        return view('pages.audit');
    }

    public function getCompany() {
        $employees = DB::table('employees_i18n')->where('locale', Session::get('locale'))->get();

        return view('pages.company')->with('employees', $employees);
    }

    public function getContacts() {
        return view('pages.contacts');
    }

    public function getFinance() {
        return view('pages.finance');
    }

    public function getLegal() {
        return view('pages.legal');
    }

    public function postAuditForm(Request $request) {
        $data = array(
            'company' => $request->company,
            'phone' => $request->phone,
            'type_of_maintanance' => $request->type_of_maintanance,
            'address' => $request->address,
            'email' => $request->email,
            'pc_programm' => $request->pc_programm,
            'client_message' => $request->client_message,
            'form_name' => $request->form_name
        );

        //hello@iters.agency
        Mail::send('emails.audit-contact', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('tavadzed@gmail.com');
            $message->subject($data['form_name'] . ' ' . $data['company']);
        });

        Session::put('success', 'Message has been sent successfully!');

        return redirect()->back();
    }

    public function postAcountForm(Request $request) {
        $data = array(
            'company' => $request->acount_company,
            'phone' => $request->acount_phone,
            'type_of_maintanance' => $request->acount_type_of_maintanance,
            'address' => $request->acount_address,
            'email' => $request->acount_email,
            'pc_programm' => $request->acount_pc_programm,
            'client_message' => $request->client_message,
            'form_name' => $request->form_name
        );

        Mail::send('emails.acount-contact', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('tavadzed@gmail.com');
            $message->subject($data['form_name'] . ' ' . $data['company']);
        });

        Session::put('success', 'Message has been sent successfully!');

        return redirect()->back();
    }

    public function postAcademyForm(Request $request) {
        $data = array(
            'company' => $request->acount_company,
            'phone' => $request->acount_phone,
            'type_of_maintanance' => $request->acount_type_of_maintanance,
            'address' => $request->acount_address,
            'email' => $request->acount_email,
            'pc_programm' => $request->acount_pc_programm,
            'client_message' => $request->client_message,
            'form_name' => $request->form_name
        );

        Mail::send('emails.academy-contact', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('info@fec.ge');
            $message->subject($data['form_name'] . ' ' . $data['company']);
        });

        Session::put('success', 'Message has been sent successfully!');

        return redirect()->back();
    }

    public function postAuditPageForm(Request $request) {
        $data = array(
            'company' => $request->acount_company,
            'phone' => $request->acount_phone,
            'type_of_maintanance' => $request->acount_type_of_maintanance,
            'address' => $request->acount_address,
            'email' => $request->acount_email,
            'pc_programm' => $request->acount_pc_programm,
            'client_message' => $request->client_message,
            'form_name' => $request->form_name
        );

        Mail::send('emails.academy-contact', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('info@fec.ge');
            $message->subject($data['form_name'] . ' ' . $data['company']);
        });

        Session::put('success', 'Message has been sent successfully!');

        return redirect()->back();
    }
}