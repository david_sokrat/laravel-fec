<footer id="footer">
    <div class="wrapper-footer">
        <div class="copyright">
            <span class="copyright-fec">Copyright</span>
            <span>all rights reserved</span>
        </div>
        <div class="footer-align">
            <div class="footer-menu">
                <nav>
                    <ul>
                        <li>
                            <a class="{{ Route::currentRouteName() == "home" ? "active-link-footer" : "" }}" href="{{ route('home', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_perf'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "company" ? "active-link-footer" : "" }}" href="{{ route('company', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_company'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "articles" ? "active-link-footer" : "" }}" href="{{ route('articles', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_news'] }}</a>
                        </li>
                        <li>
                            <a class="{{ Route::currentRouteName() == "contacts" ? "active-link-footer" : "" }}" href="{{ route('contacts', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_contacts'] }}</a>
                        </li>
                    </ul>
                </nav>
            </div>
            {{--<div class="footer-languages">
                {!! Form::open(['route' => 'language.change']) !!}
                    <button class="{{ Session::get('locale') == 'ru' ? "not-visible " : "language-select-footer lang-btn" }}" type="submit" name="lang" value="ru">RU</button>
                    <button class="{{ Session::get('locale') == 'en' ? "not-visible " : "language-select-footer lang-btn" }}" type="submit" name="lang" value="en">EN</button>
                    <button class="{{ Session::get('locale') == 'ka' || Session::get('locale') == '' ? "not-visible " : "language-select-footer lang-btn" }}" type="submit" name="lang" value="ka">KA</button>
                    <button class="{{ Session::get('locale') == 'tr' ? "not-visible " : "language-select-footer lang-btn" }}" type="submit" name="lang" value="tr">TR</button>
                {!! Form::close() !!}
            </div>--}}
        </div>
        <div class="iters">
            <img src="{{ asset('img/iters.svg') }}" alt="ITERS Logo" />
            <span>{{ $translations['created_at'] }}</span>
            <a href="https://iters.agency" target="_blank">ITERS</a>
        </div>
    </div>
</footer>