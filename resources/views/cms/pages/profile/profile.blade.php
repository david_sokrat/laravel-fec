@extends('cms.layouts.backend')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="{{ route('cms.profile') }}">{{ $translations['header_nav_profile'] }}</a></li>
                    </ol>

                    @include('partials._messages')

                    <div class="x_content" >
                        <form class="form-horizontal" enctype="multipart/form-data"  action="{{ route('cms.profile.edit') }}" method="post" id="edit-user-form">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['cms_profile_avatar'] }}</label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <div style="width: 120px">
                                        @if($admin->avatar !== '')
                                            <img src="{{ asset('storage/public/img/' . $admin->avatar) }}" alt="..." class="img-circle profile_img" style="width: 60px; height: 60px;">
                                        @else
                                            <img src="{{ asset('storage/public/img/avatar.svg') }}" alt="..." class="img-circle profile_img" style="width: 60px; height: 60px;">
                                        @endif
                                    </div>
                                    <input type="file" name="image" accept="image/*">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['user_login_name'] }}</label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <input style="border-radius: 3px;" type="text" name="name" value="{{ $admin->name }}" id="name" class="form-control" placeholder="{{ $translations['user_login_name'] }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['audit_form_company_email'] }}</label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <input style="border-radius: 3px;" type="email" autocomplete="off" id="email" name="email" value="{{ $admin->email }}" class="form-control" placeholder="{{ $translations['audit_form_company_email'] }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['cms_profile_job_title'] }}</label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <input style="border-radius: 3px;" type="text"  id="job_title" name="job_title" value="{{ $admin->job_title }}" class="form-control" placeholder="{{ $translations['cms_profile_job_title'] }}"  autocomplete="off" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['confirm_reset_password_btn'] }}</label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <input style="border-radius: 3px;" type="password" autocomplete="off" name="newpassword" class="form-control" placeholder="{{ $translations['reset_password'] }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                    <button type="submit" class="btn btn-primary">{{ $translations['cms_tab_submit'] }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

