<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;

class TranslationContent extends Model
{
    protected $table = 'translations_contents';

    public function articles()
    {
        return $this->belongsTo('App\Article');
    }
}
