<section id="company-achivments">
    <div class="wrapper">
        <div class="achivments-items">
            <div class="achivments-item first-achiv">
                <h2>100+</h2>
                <p>{{ $translations['achivment_first'] }}</p>
            </div>
            <div class="achivments-item second-achiv">
                <h2>{{ $translations['achivment_insurance_amount'] }}</h2>
                <p>{{ $translations['achivment_second'] }}</p>
            </div>
            <div class="achivments-item third-achiv">
                <h2>{{ $translations['achivment_third'] }}</h2>
                <p>{{ $translations['achivment_fourth'] }}</p>
            </div>
        </div>
    </div>
</section>