@extends('main')

@section('title', '| Contacts')

@section('stylesheets')
    <link href="{{ asset('css/contacts.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        contactsStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = true;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    @include('partials.contacts._promo-block') 
    @include('partials.contacts._contacts-telephones') 
    @include('partials.contacts._contacts-social-email') 
    @include('partials.contacts._contacts-map') 
@endsection

@section('google-maps')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgbeBBFuzfv0Xr-8XpjtFPCfwqy8ExlBk&callback=initMap" type="text/javascript"></script>
@endsection