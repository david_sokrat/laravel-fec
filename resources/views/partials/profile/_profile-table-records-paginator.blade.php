<section id="profile-table-records-paginator">
    <div class="profile-wrapper">
        <div class="pagination">
            {!! $reports->links(); !!}
        </div>
    </div>
</section>