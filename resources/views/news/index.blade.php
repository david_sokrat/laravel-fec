@extends('main')

@section('title', '| News')

@section('stylesheets')
    <link href="{{ asset('css/news.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')   
    @include('partials.articles._promo-block') 
    @include('partials.articles._all-news') 
    @include('partials.articles._news-paginator') 
@endsection