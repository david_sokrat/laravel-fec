<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;
use App\Report;
use Auth;
use Session;
use View;
use App\User;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $reports = Report::where('user_id', $userId)->paginate(12);
        $dates = array();
        $dayData = array();
        $curDate = str_replace("-", "", date('Y-m-d'));

        for ($i = 0; $i < count($reports); $i++) {
            $dates[$i] = str_replace("-", "", $reports[$i]->date);
            
            if ($dates[$i] == $curDate) {
                $dates[$i] = 'сегодня';
                $reports[$i]['date'] = $dates[$i];
            }

            if ($dates[$i] == $curDate - 1) {
                $dates[$i] = 'вчера';
                $reports[$i]['date'] = $dates[$i];
            }
        }

        View::share('dates', $dates);

        return view('reports/index')->withReports($reports)->withUser($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $id)
    {
        $report = Report::find($id);

        return view('reports.show')->withReport($report);
    }
}
