@extends('cms.layouts.backend')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="{{ route('reports.index') }}">{{ $translations['cms_sidebar_nav_all_reports'] }}</a></li>
                    </ol>

                    @include('partials._messages')

                    <div class="x_content"> <br>
                        <div class="table-responsive">
                            <a href="{{ route('reports.create') }}" class="btn btn-success btn-xs" title="Add new report"><i class="material-icons" style="line-height: 3rem;">add_circle</i></a>
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">{{ $translations['cms_item_id'] }}</th>
                                        <th class="column-title">{{ $translations['cms_item_title'] }}</th>
                                        <th class="column-title">{{ $translations['cms_item_date'] }}</th>
                                        <th class="column-title no-link last" style="text-align: end;"><span class="nobr" style="margin-right: 15px;">{{ $translations['cms_item_action'] }}</span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if(!$reports->isEmpty())

                                        @foreach($reports as $report)
                                            <tr class="even pointer">
                                                <td class=" " style="line-height: 1rem; vertical-align: middle !important;">{{ $report->id }}</td>
                                                <td class=" " style="line-height: 1rem; vertical-align: middle !important;">{{ $translations_contents[$report->title] }}</td>
                                                <td class=" " style="line-height: 1rem; vertical-align: middle !important;">{{ $report->date }}</td>
                                                <td class=" " style="padding-top: 20px !important;line-height: 1rem; text-align: end;">
                                                    {!! Form::open(['route' => ['reports.edit', $report->id], 'style' => 'display: inline-block', 'method' => 'GET']) !!}
                                                        {!! Form::submit('mode_edit', ['class' => 'material-icons btn btn-default', 'title' => 'Edit']) !!}
                                                        {{-- <span id="delete-report" class="btn btn-default material-icons" style="cursor: pointer; color: #d30004; display: inline-block;" title="Delete">
                                                            delete
                                                        </span> --}}
                                                    {!! Form::close() !!} 
                                                    {!! Form::open(['route' => ['reports.destroy', $report->id], 'style' => 'display: inline-block', 'method' => 'DELETE']) !!}
                                                        {{ csrf_field() }}
                                                        {!! Form::submit('delete', ['class' => 'material-icons btn btn-default', 'style' => 'color: #d30004;', 'title' => 'Delete']) !!}
                                                        {{-- <span id="cancel-del-report" class="btn btn-default material-icons" style="cursor: pointer; color: #6b9973; display: inline-block;" title="Cancel">
                                                            cancel
                                                        </span> --}}
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach

                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="paginator col-md-7 col-md-offset-5">{!! $reports->links(); !!}</div>
                </div>
            </div>
        </div>
    </div>

    @if(!$reports->isEmpty())
        <!-- remove user modal -->
        <div class="modal-delete-report" 
            style="
            display: none; 
            position: fixed;
            text-align: center; 
            top: 15%;
            left: 50%;
            width: 200px; 
            height: 200px; 
            background: 
            black; 
            z-index: 1000;
            background: rgba(211, 0, 4, .5);
            border: 2px solid rgb(211, 0, 4);
            border-radius: 5px;
            color: white;">
            <div class="modal-wrapper">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4 style="color: white">Are you sure?</h4>
                    {!! Form::open(['route' => ['reports.destroy', $report->id], 'style' => 'display: inline-block', 'method' => 'DELETE']) !!}
                        {{ csrf_field() }}
                        {!! Form::submit('delete', ['class' => 'material-icons btn btn-default', 'style' => 'color: #d30004;', 'title' => 'Delete']) !!}
                        {{-- <span id="cancel-del-report" class="btn btn-default material-icons" style="cursor: pointer; color: #6b9973; display: inline-block;" title="Cancel">
                            cancel
                        </span> --}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif

@stop