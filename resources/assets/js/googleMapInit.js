function initMap() {
    var uluru = {lat: 41.646046, lng: 41.626752}; 
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: uluru,
        disableDefaultUI: true,
        styles: [
            {elementType: "geometry", stylers: [{"color": "#100d20"}]},
            {elementType: "labels.icon", stylers: [{visibility: "off" }]}, 
            {elementType: "labels.text.fill", stylers: [{color: "#727478"}]}, 
            {elementType: "labels.text.stroke", stylers: [{color: "#000000"}]},
            {
                featureType: "administrative.land_parcel", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478" }]
            }, 
            {   
                featureType: "poi", 
                elementType: "geometry", 
                stylers: [{color: "#1e1f19"}]
            }, 
            { 
                featureType: "poi", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}] 
            }, 
            { 
                featureType: "poi.park", 
                elementType: "geometry", 
                stylers: [{color: "#151e1e"}] 
            }, 
            { 
                featureType: "poi.park", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}]
            },
            { 
                featureType: "road", 
                elementType: "geometry", 
                stylers: [{color: "#282c2e"}] 
            }, 
            { 
                featureType: "road.arterial", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}]
            }, 
            { 
                featureType: "road.highway", 
                elementType: "geometry", 
                stylers: [{color: "#282c2e"}]
            }, 
            { 
                featureType: "road.highway", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}] 
            }, 
            { 
                featureType: "road.local", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}] 
            }, 
            { 
                featureType: "transit.line", 
                elementType: "geometry", 
                stylers: [{color: "#282c2e"}] 
            }, 
            { 
                featureType: "transit.station", 
                elementType: "geometry", 
                stylers: [{color: "#282c2e"}] 
            }, 
            { 
                featureType: "water", 
                elementType: "geometry", 
                stylers: [{color: "#182331"}]
            }, 
            { 
                featureType: "water", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#727478"}]
            } 
        ]
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(41.643519, 41.626026),
        map: map,
        icon: '/img/fec-pin.svg'
    });

    google.maps.event.addListener(marker, 'click', function() {
        window.location.href = 'https://www.fec.ge/ka';
    });
}