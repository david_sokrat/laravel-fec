@if(Auth::guest())

    <section id="auth-modal">
        <div class="auth-modal-wrapper">
            <div class="auth-icon-modal active">
                <div class="auth-modal-close"></div>
            </div>

            @include('auth.login')
              
        </div>
    </section>
    
@endif