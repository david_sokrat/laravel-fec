@extends('backend.layouts.backend')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">


                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>
                        <li>{{tc('other.pages')}}</li>

                        <li class="active">{{tc('georgia')}} </li>


                    </ol>



                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                    @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                    @endif
                    <div class="x_content" > <br>






                        <form id="demo-form2" action="{{route('cms.georgia.save',$geo->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">

                            {{ csrf_field() }}
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">

                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    @php $activator = 0; @endphp
                                    @foreach($locales as $l)
                                        <li role="presentation" @if($activator == 0) class="active" @else class="" @endif>
                                            <a href="#{{$l->locale}}" data-toggle="tab" aria-expanded="true">{{$l->name}}</a>
                                        </li>
                                        @php $activator++ @endphp
                                    @endforeach
                                </ul>

                                <div id="myTabContent" class="tab-content">

                                    @php $activator = 0; @endphp
                                    @foreach($locales as $loc)
                                        <div role="tabpanel" class="tab-pane fade @if($activator == 0) active in  @endif" id="{{$loc->locale}}">



                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{tc('name')}}
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="first-name" name="name_{{$loc->locale}}" required="required" value="{{($geo->translate($loc->locale))? $geo->translate($loc->locale)->name : null}}" class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Meta Keywords
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <textarea    class="form-control" name="meta_keywords_{{$loc->locale}}" >{{($geo->translate($loc->locale))? $geo->translate($loc->locale)->meta_keywords : null}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Meta Description
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <textarea    class="form-control" name="meta_description_{{$loc->locale}}" >{{($geo->translate($loc->locale))? $geo->translate($loc->locale)->meta_description : null}}</textarea>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{tc('text')}}
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea    class="form-control" id="ckeditor_{{$loc->locale}}" name="text_{{$loc->locale}}" >{{($geo->translate($loc->locale))? $geo->translate($loc->locale)->text : null}}</textarea>
                                                </div>
                                            </div>



                                        </div>


                                        @php $activator++ @endphp
                                    @endforeach
                                </div> </div>
                            <div class="ln_solid"></div>



                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >cover <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="cover[]" class="form-control col-md-7 col-xs-12" id="input" accept="image/x-png,image/gif,image/jpeg">
                                    <div class="ln_solid"></div>
                                    <img src="/files/shares/georgia/{{$geo->cover}}"  width="90" height="50">
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">{{tc('save')}}</button>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('scripts')
    <script>
        @foreach($locales as $loc)
        CKEDITOR.replace( 'ckeditor_{{$loc->locale}}' );

        @endforeach


    </script>
@endsection




