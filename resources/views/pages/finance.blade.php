@extends('main')

@section('title', '| Finance')

@section('stylesheets')
    <link href="{{ asset('css/finance.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = true;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('canvas')
    <canvas id="canvas0"></canvas>
@endsection

@section('content')
    @include('partials.finance._tax-dispute-promo') 
    @include('partials.finance._tax-dispute-text-content') 
    @include('partials.finance._tax-order-form') 
@endsection