<section id="promo-block-article">
    <div class="wrapper">
        <h1>{{ $translations_contents[$new->title] }}</h1>
        <div class="date-of-article">
            <p>{{ $new->date }}</p>
        </div>
    </div>
</section>