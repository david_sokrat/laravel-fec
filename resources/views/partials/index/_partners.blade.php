<section id="partners">
    <div class="wrapper">
        <h4>{{ $translations['work_with_us'] }}</h4>
        <div class="owl-carousel owl-theme owl-loaded">
            <div class="owl-stage-outer owl-partners">
                <div class="partners-items owl-stage">
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/struijk.jpg" y="38" x="15" width="70px" height="30px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img1" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/hgroup-logo.png" y="28" x="25" width="50px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img1)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img2" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/weexpress.jpg" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img2)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img3" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/exp-group-logo.jpg" y="28" x="10" width="80px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img3)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img4" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/intermedica-logo.png" y="28" x="10" width="80px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img4)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img5" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/bravo-logo.jpg" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img5)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img6" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/ajad-logo.jpg" y="28" x="13" width="70px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img6)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img7" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/city-life-logo.jpg" y="28" x="15" width="70px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img7)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img8" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/dolphin-logo.jpg" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img8)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img9" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/victoria-logo.jpg" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img9)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img10" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/mon-ami-logo.jpg" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img10)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img11" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/gemo-logo.jpg" y="20" x="20" width="60px" height="60px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img11)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img12" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/transload-logo.jpg" y="28" x="15" width="70px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img12)"/>
                            </svg>
                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="partners-item">
                            <svg viewbox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <pattern id="img13" patternUnits="userSpaceOnUse" width="160" height="160">
                                        <image xlink:href="/img/geo-fibre-logo.png" y="28" x="20" width="60px" height="50px"></image>
                                    </pattern>
                                </defs>
                                <polygon id="hex" points="50 1 95 25 95 75 50 99 5 75 5 25" fill="url(#img13)"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prev-partners-btn"></div>
            <div class="next-partners-btn"></div>
        </div>
    </div>
</section>