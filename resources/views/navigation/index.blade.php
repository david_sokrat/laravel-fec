@extends('main')

@section('title', '| Year navigation')

@section('stylesheets')
    <link href="{{ asset('css/news.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/tax-dispute.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
    <script type="text/javascript">
        mainStatus = false;
        legalStatus = false;
        auditStatus = false;
        financeStatus = false;
        academyStatus = false;
        taxDisputeStatus = false;
        companyPageStatus = false;
        textGlStatus = false;
        googleMapStatus = false;
        //Change value of variables for specific pages
    </script>
@endsection

@section('content')
    <section id="promo-block">
        <div class="wrapper">
            <h1>Новости</h1>
            <div class="year-of-news-items">

                @foreach ($navigation as $nav)
                    <div class="year-of-news-item">
                        <a id="year{{ $nav->year }}" herf=##>{{ $nav->year }}</a>
                    </div>
                @endforeach
            
            </div>
            {!! Form::open(['route' => 'navigation.store', 'method' => 'POST']) !!}
                {{ Form::text('year', 'Введите год', ['class' => 'user-name']) }}
                {{ Form::submit('Добавить', ['class' => 'btn-create']) }}
            {!! Form::close() !!}
        </div>
    </section>
@endsection