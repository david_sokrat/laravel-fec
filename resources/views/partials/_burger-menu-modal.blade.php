<section id="burger-menu-modal">
    <div class="modal-wrapper">
        <div class="burger-icon-modal active">
            <div class="burger-modal"></div>
        </div>
        {{--<div class="languages-modal">
            {!! Form::open(['route' => 'language.change']) !!}
                <button class="{{ Session::get('locale') == 'ru' ? "not-visible " : "language-select-burger lang-btn" }}" type="submit" name="lang" value="ru">RU</button>
                <button class="{{ Session::get('locale') == 'en' ? "not-visible " : "language-select-burger lang-btn" }}" type="submit" name="lang" value="en">EN</button>
                <button class="{{ Session::get('locale') == 'ka' || Session::get('locale') == '' ? "not-visible " : "language-select-burger lang-btn" }}" type="submit" name="lang" value="ka">KA</button>
                <button class="{{ Session::get('locale') == 'tr' ? "not-visible " : "language-select-burger lang-btn" }}" type="submit" name="lang" value="tr">TR</button>
            {!! Form::close() !!}
        </div>--}}
        <div class="header-menu-modal">
            <nav>
                <ul>
                    <li>
                        <a class="{{ Route::currentRouteName() == "home" ? "active-link" : "" }}" href="{{ route('home', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_perf'] }}</a>
                    </li>
                    <li>
                        <a class="{{ Route::currentRouteName() == "company" ? "active-link" : "" }}" href="{{ route('company', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_company'] }}</a>
                    </li>
                    <li>
                        <a class="{{ Route::currentRouteName() == "articles" ? "active-link" : "" }}" href="{{ route('articles', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_news'] }}</a>
                    </li>
                    <li>
                        <a class="{{ Route::currentRouteName() == "contacts" ? "active-link" : "" }}" href="{{ route('contacts', ['lang' => Session::get('locale')]) }}">{{ $translations['header_nav_contacts'] }}</a>
                    </li>

                    @php 
                        $cur_phone;

                        if(Session::get('city') == 'batumi') {
                            $cur_phone = '+995 422 27 77 11';
                        } else {
                            $cur_phone = '+995 555 55 27 77';
                        }
                    @endphp

                    <li>
                        <a class="callback-phone-burger" href="tel:{{ $cur_phone }}">{{ $cur_phone }}</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="company-directions-modal">
            <p>{{ $translations['subheader_label'] }}</p>
        </div>
        <div class="directions-items-modal">
            <nav class="sub-header-menu">
                <ul>
                    <li><a class="{{ Route::currentRouteName() == "finance" ? "active-link" : "" }}" href="{{ route('finance', ['lang' => Session::get('locale')]) }}">{{ $translations['subheader_nav_finance'] }}</a></li>
                    <li><a class="{{ Route::currentRouteName() == "audit" ? "active-link" : "" }}" href="{{ route('audit', ['lang' => Session::get('locale')]) }}">{{ $translations['subheader_nav_audit'] }}</a></li>
                    <li><a class="{{ Route::currentRouteName() == "legal" ? "active-link" : "" }}" href="{{ route('legal', ['lang' => Session::get('locale')]) }}">{{ $translations['subheader_nav_legal'] }}</a></li>
                    <li class="academy-li-modal"><a class="{{ Route::currentRouteName() == "academy" ? "active-link" : "" }}" href="{{ route('academy', ['lang' => Session::get('locale')]) }}">{{ $translations['subheader_nav_academy'] }}</a></li>
                    <li class="tax-dispute-li-modal"><a class="{{ Route::currentRouteName() == "tax-dispute" ? "active-link" : "" }}" href="{{ route('tax-dispute', ['lang' => Session::get('locale')]) }}">{{ $translations['subheader_nav_tax_dispute'] }}</a></li>
                </ul>
            </nav>
        </div>
        <div id="town-of-interest-modal">
            <div class="toggle-town-modal">
                <form id="toggle-town-burger" class="toggle-town" style="display: flex;" action="{{ route('set-contacts', Session::get('locale')) }}" method="POST">
                    {{ csrf_field() }} 

                    @php $cur_city = Session::get('city') @endphp
                    <h4 class="{{ $cur_city == 'batumi' ? "batumi-modal active-town" : "batumi-modal" }}">{{ $translations['subheader_batumi'] }}</h4>
                    <label class="switch-town-modal">
                        <input name="cur_city" value="{{ $cur_city }}" type="hidden">
                        <button 
                            class="toggle-city-checkbox" 
                            type="submit" 
                            style="position: absolute;
                            width: 100%;
                            height: 100%;
                            opacity: 0;
                            z-index: 10000;
                            left: 0;
                            cursor: pointer;">
                        </button>
                        <span class="town-slider-modal">
                            <span class="{{ $cur_city == 'tbilisi' ? "town-slider-checked-modal move-city-toggler-burger" : "town-slider-checked-modal" }}"></span>
                        </span>
                    </label>
                    <h4 class="{{ $cur_city == 'tbilisi' ? "tbilisi-modal active-town" : "tbilisi-modal" }}">{{ $translations['subheader_tbilisi'] }}</h4>
                </form>
                {{-- <div id="burger-loading"></div>
                <div id="loader-container" class="loader">
                    <div id="loader-icon"></div>
                    <div class="loading-text">loading...</div>
                </div> --}}
            </div>
        </div>
    </div>
</section>