<section id="contacts-social-email">
    <div class="wrapper">
        <div class="contacts-social-items">
            <div class="contacts-social-item">
                <p>{{ $translations['audit_form_company_email'] }}</p>
                <div class="contacts-email-link">

                    @php $cur_email;
                        if (Session::get('city') == 'batumi') {
                            $cur_email = 'info@fec.ge';
                        } else {
                            $cur_email = 'tbilisi@fec.ge';
                        }
                    @endphp
                    <a href="mailto:{{ $cur_email }}">{{ $cur_email }}</a>
                </div>
            </div>
            <div class="contacts-social-item">
                <p>{{ $translations['contacts_social_network'] }}</p>
                <div class="contacts-facebook-link">
                    <a href="https://www.facebook.com/fec.ge/">{{ $translations['contacts_facebook'] }}</a>
                </div>
            </div>
            <div class="contacts-social-item">
                <p>{{ $translations['contacts_office_location'] }}</p>
                <div class="contacts-address">
                    @if(Session::get('city') == 'batumi')
                        <address>{{ $translations['contacts_offise_address'] }}</address>
                    @else
                        <address>{{ $translations['contacts_offise_address_tbilisi'] }}</address>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>