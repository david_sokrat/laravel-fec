/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/cms.js":
/***/ (function(module, exports) {

$(document).ready(function () {
    $('#open-reg-form').click(function (e) {
        e.preventDefault();

        $('#register-modal').fadeIn(350);
    });

    $('.auth-icon-modal-cms').click(function (e) {
        e.preventDefault();

        $('#register-modal').fadeOut(350);
    });

    $('#delete-report').click(function (e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-report').fadeIn(350);
    });

    $('#cancel-del-report').click(function (e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-report').fadeOut(350);
    });

    $('#delete-article').click(function (e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-article').fadeIn(350);
    });

    $('#cancel-del-article').click(function (e) {
        e.preventDefault();
        var that = this;

        $('.modal-delete-article').fadeOut(350);
    });

    $('.language-select-cms').on('change', function () {
        $(this).closest('form').submit();
    });
});

//Chache clear srcipt
$('.cache_clear').click(function () {
    var data = {
        _token: '{{csrf_token()}}'
    };

    $.post('{{ route(\'cms.settings.cache.clear\') }}', data).done(function (data) {
        location.reload();
    });
});

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/cms.js");


/***/ })

/******/ });