<section id="contacts-telephones">
    <div class="wrapper">
        <p>{{ $translations['contacts_tel_title'] }}</p>
        <div class="contacts-tel-items">

            @php $cur_phone;
                if (Session::get('city') == 'batumi') {
                    $cur_phone = '+995 422 27 77 11';
                    $cur_phone2 = '+995 577 57 57 29';
                    $cur_phone3 = '+995 599 26 25 68';
                    $cur_phone4 = '+995 577 40 15 04';
                } else {
                    $cur_phone = '+995 555 55 27 77';
                    $cur_phone2 = '+995 577 57 57 29';
                    $cur_phone3 = '+995 599 26 25 68';
                    $cur_phone4 = '+995 577 40 15 04';
                }
            @endphp
            
            <div class="contacts-tel-item">
                <a href="tel:{{ $cur_phone }}">{{ $cur_phone }}</a>
            </div>
            <div class="contacts-tel-item">
                <a href="tel:{{ $cur_phone2 }}">{{ $cur_phone2 }}</a>
            </div>
            <div class="contacts-tel-item">
                <a href="tel:{{ $cur_phone3 }}">{{ $cur_phone3 }}</a>
            </div>
            <div class="contacts-tel-item">
                <a href="tel:{{ $cur_phone4 }}">{{ $cur_phone4 }}</a>
            </div>
        </div>
    </div>
</section>