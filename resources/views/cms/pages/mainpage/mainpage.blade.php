@extends('cms.layouts.backend')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="x_title">
                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{ route('cms.dashboard') }}"><i class="fa fa-home"></i></a></li>
                        {{-- <li>{{ trans('other.pages') }}</li>
                        <li class="active">{{ trans('main.page') }}</li> --}}
                    </ol>

                    @include('partials._messages')

                    <div class="x_content"><br>
                        <form id="demo-form2" action="" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['cms_mainpage_number_clients'] }}</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" readonly name="h1_" required="required" value="{{ count($users) }}" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" >{{ $translations['cms_mainpage_number_reports'] }}</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" readonly name="slogan_" required="required" value="" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ $translations['cms_mainpage_list_clients'] }}</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control col-md-7 col-xs-12" name="navigation_id">
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop





