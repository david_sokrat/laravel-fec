<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Locales;
use App\User;
use App\Admin;
use Session;
use Illuminate\Support\Facades\DB;

class MainpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locales = Locales::all();
        $users = User::all();
        $admin_id = Session::get('admin_obj');
        $admin = Admin::find($admin_id);
        $avatar = $admin['avatar'];
        $name = $admin['name'];
        $job = $admin['job_title'];

        return view('cms/pages/mainpage/mainpage')
            ->withLocales($locales)
            ->withUsers($users)
            ->withAvatar($avatar)
            ->withName($name)
            ->withJob($job);
    }
}
