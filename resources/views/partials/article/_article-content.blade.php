<section id="article-content">
    <div class="wrapper">
        <div class="article-text">
            <p>{!! strip_tags($translations_contents[$new->body], '<a><iframe><div><img>') !!}</p>
        </div>
        <div class="back-to-news-link">
            <a href="{{ route('articles', Session::get('locale')) }}"><img src="{{ asset('img/back-to-news-arrow.svg') }}" alt="Back to all news" />{{ $translations['back_to_list'] }}</a>
        </div>
    </div>    
</section>