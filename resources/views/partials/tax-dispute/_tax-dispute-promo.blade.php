<section id="tax-dispute-promo">
    <script id="template4" type="notjs">
        <div class="scene4"></div>
    </script>
    <div id="content4"></div>
    <div class="wrapper">
        <div class="red-brilliant brl-item">
            <div class="brl-wrap">
                <!-- <img src="{{ asset('img/finance.svg') }}" alt="Maintenance and accounting consulting" />-->
            </div>
            <div class="rd-text-text-dispute">
                <h1>{{ $translations['tax_dispute.main'] }}</h1>
                <p>{{ $translations['tax_dispute.main_text'] }}</p>
            </div>
        </div>
    </div>
</section>