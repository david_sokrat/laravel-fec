@if (Auth::guard('web')->check())
    <div class="input-feilds">
        <div class="user-email">
            You are Logged In as a <strong>USER</strong>
        </div>
    </div>
@else
    <div class="input-feilds">
        <div class="user-email">
            You are Logged Out as a <strong>USER</strong>
        </div>
    </div>
@endif

@if (Auth::guard('admin')->check())
    <div class="input-feilds">
        <div class="user-email">
            You are Logged In as a <strong>ADMIN</strong>
        </div>
    </div>
@else
    <div class="input-feilds">
        <div class="user-email">
            You are Logged Out as a <strong>ADMIN</strong>
        </div>
    </div>
@endif
