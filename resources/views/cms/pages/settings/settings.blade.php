@extends('backend.layouts.backend')


@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">

                    <ol class="breadcrumb bread-primary ">
                        <li><a href="{{route('cms.dashboard')}}"><i class="fa fa-home"></i></a></li>
                        <li class="active">{{tc('settings')}}</li>
                    </ol>

                </div>
                <div class="x_content">
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("success") !!}
                        </div>
                @elseif(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            {!! Session::get("error") !!}
                        </div>
                @endif




                <!-- start accordion -->
                    <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">

                        <!-- main settings -->
                        <div class="panel">
                            <a class="panel-heading collapse" role="tab" id="headingOne" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                               aria-controls="collapseOne">
                                <h4 class="panel-title">{{tc('main.settings')}} </h4>
                            </a>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">


                                    <a class="btn btn-app"  data-toggle="modal"
                                       data-target=".modal-working">
                                        @if($devmode->value == 0)
                                        <span class="badge bg-red">OFF</span>
                                        @else
                                            <span class="badge bg-blue">ON</span>
                                        @endif
                                        <i class="fa fa-power-off"></i><p>Dev Mode</p>
                                    </a>



                                    <a class="btn btn-app" id="minify">
                                        @if($minify->value == 1)  <span class="badge bg-blue">ON</span>
                                        @else <span class="badge bg-red">OFF</span> @endif
                                     <i class="fa fa-code"></i>  <p>Minify HTML</p>
                                    </a>










                                </div>
                            </div>
                        </div>

                        <!-- users settings -->
                        <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                               aria-controls="collapseThree">
                                <h4 class="panel-title">{{tc('user.management')}}</h4>
                            </a>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">


                                    <div class="table-responsive">
                                        <a href="" class="btn btn-success btn-xs" id="axaliuser" data-toggle="modal"
                                           data-target=".modal-edituser"> <i class="material-icons">add_circle</i> </a>
                                        <table class="table table-striped jambo_table bulk_action">

                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title">Id</th>
                                                <th class="column-title">{{tc('name')}}</th>
                                                <th class="column-title">{{tc('job.title')}}</th>
                                                <th class="column-title">{{tc('mail')}}</th>
                                                <th class="column-title no-link last"><span class="nobr">{{tc('action')}}</span>
                                                </th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($users as $user)
                                                <tr class="even pointer">
                                                    <td class=" ">{{$user->id}}</td>
                                                    <td class=" ">{{$user->name}}</td>
                                                    <td class=" ">{{$user->job_title}}</td>
                                                    <td class=" ">{{$user->email}}</td>

                                                    <td class=" ">
                                                        <a href="" class="edituserdata" data-user="{{$user->id}}"
                                                           data-toggle="modal" data-target=".modal-edituser"><i
                                                                    class="material-icons">mode_edit</i></a> &nbsp;
                                                        @if($user->id != Auth::id())
                                                        <a href="" class="deluser" style="color: #d30004;"
                                                           data-user="{{$user->id}}" data-toggle="modal"
                                                           data-target=".modal-deluser"><i
                                                                    class="material-icons">delete</i></a> @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <!-- language settings -->
                        <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                               aria-controls="collapseTwo">
                                <h4 class="panel-title">{{tc('language.settings')}}</h4>
                            </a>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">


                                    <div class="table-responsive">
                                        <a href="" class="btn btn-success btn-xs" id="axalilang" data-toggle="modal" data-target=".modal-editlang"> <i
                                                    class="material-icons">add_circle</i> </a>
                                        <table class="table table-striped jambo_table bulk_action">

                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title">{{tc('lang')}}</th>
                                                <th class="column-title">{{tc('key')}}</th>
                                                <th class="column-title">{{tc('status')}}</th>
                                                <th class="column-title">{{tc('default.main')}}</th>
                                                <th class="column-title">{{tc('default.cms')}}</th>
                                                <th class="column-title no-link last"><span class="nobr">{{tc('action')}}</span>
                                                </th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($langs as $lang)
                                                <tr class="even pointer">
                                                    <td class=" ">{{$lang->name}}</td>
                                                    <td class=" ">{{$lang->locale}}</td>
                                                    <td class=" ">
                                                        @if($lang->active ==1)<i class="material-icons"
                                                                                 style="color: #1fdd00;">check_circle</i>@else
                                                            <i class="material-icons" style="color: #c04b05;">highlight_off</i> @endif
                                                    </td>
                                                    <td class=" ">@if($lang->default ==1)<i class="material-icons"
                                                                                            style="color: #1fdd00;">check_circle</i>@else
                                                            <i class="material-icons" style="color: #c04b05;">highlight_off</i> @endif
                                                    </td>
                                                    <td class=" ">@if($lang->admin_default ==1)<i class="material-icons"
                                                                                                  style="color: #1fdd00;">check_circle</i>@else
                                                            <i class="material-icons" style="color: #c04b05;">highlight_off</i> @endif
                                                    </td>
                                                    <td class="">
                                                        <a href="" class="editlang" data-langid="{{$lang->id}}"
                                                           data-toggle="modal" data-target=".modal-editlang"><i class="material-icons">mode_edit</i></a> &nbsp;

                                                        @if($lang->admin_default ==0 and $lang->default == 0)
                                                            <a href="" style="color: #d30004;" class="dellang" style="color: #d30004;"
                                                               data-lid="{{$lang->id}}" data-toggle="modal"
                                                               data-target=".modal-dellang"><i class="material-icons">delete</i></a>
                                                        @endif
                                                    </td>

                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- cms translations settings -->
                        <div class="panel">
                            <a class="panel-heading collapse" role="tab" id="headingOne" data-toggle="collapse"
                               data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                               aria-controls="collapseFour">
                                <h4 class="panel-title">{{tc('trans.for.cms')}}</h4>
                            </a>
                            <div id="collapseFour" class="panel-collapse collapse @if(isset($_REQUEST['page'])) in @elseif(isset($_REQUEST['search'])) in @endif" role="tabpanel"
                                 aria-labelledby="headingOne" aria-expanded="@if(isset($_REQUEST['page']))true @else false @endif" style="height: 0px;">
                                <div class="panel-body">






                                    <div class="x_content">

                                        <a href="" class="btn btn-success btn-xs" id="axalitranslate" data-toggle="modal"
                                           data-target=".modal-edittranslate"> <i class="material-icons">add_circle</i> </a>

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">

                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                @php $activator = 0; @endphp
                                                @foreach($locales as $l)
                                                    <li role="presentation" @if($activator == 0) class="active" @else class="" @endif>
                                                        <a href="#{{$l->locale}}" data-toggle="tab" aria-expanded="true">{{$l->name}}</a>
                                                    </li>
                                                    @php $activator++ @endphp
                                                @endforeach
                                            </ul>

                                            <div id="myTabContent" class="tab-content">

                                                <form action="{{route('cms.settings')}}" method="get" class="form-horizontal form-label-left input_mask">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                                        <input type="text" class="form-control" name="search" value="@if(isset($_REQUEST['search'])){{$_REQUEST['search']}}@endif" id="inputSuccess3" placeholder="search translate">
                                                    </div>@if(isset($_REQUEST['search']))<a href="{{route('cms.settings')}}"><i class="fa fa-close"></i></a>&nbsp; @endif <button type="submit" class="btn btn-success btn-sm">{{tc('search')}}</button>


                                                </form>









                                                @php $activator = 0; @endphp
                                                @foreach($locales as $loc)
                                                    <div role="tabpanel" class="tab-pane fade @if($activator == 0) active in  @endif" id="{{$loc->locale}}">






                                                        <div class="table-responsive">

                                                            <table class="table table-striped jambo_table bulk_action">
                                                                    <thead>
                                                                <tr class="headings">
                                                                    <th class="column-title">Id</th>
                                                                    <th class="column-title">{{tc('key')}}</th>
                                                                    <th class="column-title">{{tc('translate')}}</th>
                                                                    <th class="column-title no-link last"><span class="nobr">{{tc('action')}}</span>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                @foreach($trans as $p)
                                                                    @if($p->locale == $loc->locale)

                                                                <tr class="even pointer" {{notranscms($p->trans_key)}} >
                                                                    <td class="">{{$p->id}}</td>
                                                                    <td class="">{{$p->trans_key}}</td>
                                                                    <td class="">{{$p->value}}</td>
                                                                    <td class="">
                                                                        <a href="" id="edittrans" class="edittrans" data-tkey="{{$p->trans_key}}"
                                                                           data-toggle="modal" data-target=".modal-edittranslate"><i
                                                                                    class="material-icons">mode_edit</i></a> &nbsp;

                                                                        <a href="" id="deltrans" class="deltrans" style="color: #d30004;"
                                                                           data-tkey="{{$p->trans_key}}" data-toggle="modal"
                                                                           data-target=".modal-deltrans"><i
                                                                                    class="material-icons">delete</i></a>
                                                                    </td>
                                                                </tr>

                                                                    @endif

                                                                @endforeach




                                                                </tbody>
                                                            </table>
                                                        </div>





                                                </div>
                                                    @php $activator++ @endphp
                                                @endforeach
                                                {!! $trans->render() !!}









                                            </div>
                                        </div>

                                    </div>




                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <!-- end of accordion -->


            </div>
        </div>
    </div>




    <!-- edituer modal -->
    <div class="modal fade modal-edituser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{tc('user.management')}}</h4>
                </div>
                <div class="modal-body">


                    <form class="form-horizontal" autocomplete="off"  action="{{route('cms.settings.edit.user.save')}}" method="post"
                          id="edit-user-form">
                        <input type="hidden" name="userid" id="euserid"/>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('name')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="name" id="euname" class="form-control" placeholder="Add Name"
                                       required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('mail')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="email" autocomplete="off" id="euemail" name="email" class="form-control" placeholder="Add Mail"
                                       required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('job.title')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text"  id="eujob_title" name="job_title" class="form-control"
                                       placeholder="Add Job Title"  autocomplete="off" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('password')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="password" autocomplete="off" name="password" class="form-control"
                                       placeholder="Set New Password">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('permissions')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-16">
                                <select class="js-example-basic-multiple form-control" style="width: 100%"
                                        name="rules[]" multiple="multiple" id="eurules">

                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->rule}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                            <div class="col-md-9 col-sm-9 col-xs-16">
                                <button type="submit" class="btn btn-primary edituser">{{tc('save')}}</button>
                            </div>
                        </div>



                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnclose" data-dismiss="modal">{{tc('close')}}</button>

                </div>
            </div>
        </div>
    </div>



    <!-- remove user modal -->
    <div class="modal fade modal-deluser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">{{tc('remove.user')}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('cms.settings.user.delete')}}" method="post">
                        <input type="hidden" name="deluserid" id="deleuserid"/>
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{tc('close')}}</button>
                        <button type="submit" class="btn btn-primary btn-danger">{{tc('remove')}}</button>
                    </form>
                </div>

            </div>
        </div>
    </div>




    <!-- editlang modal -->
    <div class="modal fade modal-editlang" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">{{tc('language.settings')}}</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal form-label-left" action="{{route('cms.settings.edit.lang.save')}}" method="post">

                        <input type="hidden"  name="langid" id="langid">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('name')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="name" id="langname" placeholder="Add Name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('key')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="key" id="langkey" placeholder="Add Key" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('parameters')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="">
                                    <label>
                                        <input type="checkbox"  name="active" id="langactive" /> {{tc('active')}} &nbsp;
                                    </label>
                                </div>

                                <div class="" >
                                    <label >
                                        <input type="checkbox" name="mdefault" id="mdefaultlang" /> {{tc('default.main')}} &nbsp;

                                    </label>
                                </div>

                                <div class="" >
                                    <label >
                                        <input type="checkbox" name="cdefault"  id="cdefaultlang"/> {{tc('default.cms')}} &nbsp;

                                    </label>
                                </div>

                            </div>
                        </div>


                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                               <button type="submit" class="btn btn-success">{{tc('save')}}</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnclose" data-dismiss="modal">{{tc('close')}}</button>

                </div>
            </div>
        </div>
    </div>


    <!-- remove lang modal -->
    <div class="modal fade modal-dellang" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">{{tc('remove.lang')}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('cms.settings.lang.delete')}}" method="post">
                        <input type="hidden" name="dellangid" id="dellangid"/>
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{tc('close')}}</button>
                        <button type="submit" class="btn btn-primary btn-danger">{{tc('remove')}}</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- edit translations modal -->
    <div class="modal fade modal-edittranslate" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit Language</h4>
                </div>
                <div class="modal-body">

                    <form class="form-horizontal form-label-left" action="{{route('cms.settings.edit.translations.save')}}" method="post">

                        <input type="hidden"  name="oldtranskey" id="oldtranskey">
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{tc('key')}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="key" id="transkey" placeholder="Add Key" required>
                            </div>
                        </div>


                        @foreach($locales as $l)
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$l->name}}</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="trans_{{$l->locale}}" id="trans_{{$l->locale}}" placeholder="Add translation" required>
                            </div>
                        </div>
                        @endforeach





                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">{{tc('save')}}</button>
                            </div>
                        </div>

                    </form>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnclose" data-dismiss="modal">{{tc('close')}}</button>

                </div>
            </div>
        </div>
    </div>

    <!-- remove translate modal -->
    <div class="modal fade modal-deltrans" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">{{tc('translation.remove')}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('cms.settings.translations.remove')}}" method="post">
                        <input type="hidden" name="trkey" id="deltransk" class="deltransk" />
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{tc('close')}}</button>
                        <button type="submit" class="btn btn-primary btn-danger">{{tc('remove')}}</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- maintenance mode modal -->
    <div class="modal fade modal-working" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">{{tc('maintenance.mode')}}</h4>
                </div>
                <div class="modal-body">

                    <form action="{{route('cms.settings.site.devmode')}}" method="post">
                        <div class="form-group">
                            <label class="control-label" for="maintenance">{{tc('enable.disable')}}</label>
                            <input type="checkbox" class="control-label" name="enable" id="maintenance" @if($devmode->value == 1) checked @endif>
                        </div>



                        <label class="control-label" for="tags_1">{{tc('allowed.ips')}}: </label>
                        <input id="tags_1" type="text" class="tags form-control" name="ips" value="@foreach($ips as $ip) {{$ip->ip}}, @endforeach" />
                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>


                        {{ csrf_field() }}




                        <button type="button" class="btn btn-default" data-dismiss="modal">{{tc('close')}}</button>
                        <button type="submit" class="btn btn-success" >{{tc('save')}}</button>

                    </form>


                </div>

            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.edituserdata').click(function (e) {
                e.preventDefault();
                var that = this;
                $.post('{{route('cms.settings.edit.user')}}', {
                    userid: $(this).data('user'),
                    _token: '{{csrf_token()}}'
                }).done(function (data) {
                    $('#euserid').val(data.user.id);
                    $('#euname').val(data.user.name);
                    $('#euemail').val(data.user.email);
                    $('#eujob_title').val(data.user.job_title);
                    var selects = document.getElementById('eurules').options;
                    for (var u = 0; u < data.roles.length; u++) {
                        for (var i = 0; i < selects.length; i++) {
                            if (data.roles[u].id == selects[i].value) {

                                selects[i].selected = true;
                            }
                        }
                    }
                    var newOption = new Option('', '', false, false);
                    $('#eurules').append(newOption).trigger('change');
                });
            });
        });

        $(document).ready(function () {
            $('.js-example-basic-multiple').select2({
                "id": "value attribute",
                "element": HTMLOptionElement
            });
        });

        $('.btnclose').click(function () {
            location.reload();

        });

        $('#axaliuser').click(function () {
           $('#euserid').val('');
            $(".edituser").addClass("addNewUser");
            $(".addNewUser").hide();


        });

        $('#axalilang').click(function () {
            $('#langid').val('');
            $('#langname').val('');
            $('#langkey').val('');
        });

        $(document).ready(function () {
            $('.deluser').click(function (e) {
                e.preventDefault();
                var that = this;
                $('#deleuserid').val($(this).data('user'));
            });
        });

        $(document).ready(function () {
            $('.dellang').click(function (e) {
                e.preventDefault();
                var that = this;
                $('#dellangid').val($(this).data('lid'));
            });
        });

        $(document).ready(function () {
            $('.editlang').click(function (e) {
                e.preventDefault();
               $.post('{{route('cms.settings.edit.lang')}}', {
                    langid: $(this).data('langid'),
                    _token: '{{csrf_token()}}'
                }).done(function (data) {
                    console.log(data);
                   $('#langid').val(data.lang.id);
                   $('#langname').val(data.lang.name);
                   $('#langkey').val(data.lang.locale);
                   if(data.lang.active == 1){
                       $('#langactive').prop('checked', true);
                   }
                   if(data.lang.default == 1){
                       $('#mdefaultlang').prop('checked', true);
                   }
                   if(data.lang.admin_default == 1){
                       $('#cdefaultlang').prop('checked', true);
                   }
                });
            });
        });

        $('#minify').click(function () {

                    if($(this).find('span').text() === "ON"){
                        $.post('{{route('cms.settings.html.minify')}}', {
                            go: '0',
                            _token: '{{csrf_token()}}'
                        }).done(function (data) {

                        });
                        $(this).find('span').removeClass("badge bg-blue").addClass( "badge bg-red" );
                        $(this).find('span').text('OFF');
                    } else {
                        $.post('{{route('cms.settings.html.minify')}}', {
                            go: '1',
                            _token: '{{csrf_token()}}'
                        }).done(function (data) {

                        });
                        $(this).find('span').removeClass("badge bg-red").addClass( "badge bg-blue" );
                        $(this).find('span').text('ON');



                    }
        });

        $(document).ready(function () {
            $('.edittrans').click(function (e) {
                e.preventDefault();

                  $.post('{{route('cms.settings.edit.translations')}}', {
                    key: $(this).data('tkey'),
                    _token: '{{csrf_token()}}'
                }).done(function (data) {

                    $('#oldtranskey').val(data[0].trans_key);
                    $('#transkey').val(data[0].trans_key);

                      for (let i=0; i<data.length; i++) {

                          @foreach($locales as $l)

                          var locale = '{{$l->locale}}';

                          if(data[i].locale == locale){

                              $('#trans_{{$l->locale}}').val(data[i].value);
                          }

                          @endforeach


                      }

                });
            });
        });


        $('#axalitranslate').click(function () {
            $('#oldtranskey').val('');
            $('#transkey').val('');

                    @foreach($locales as $l)
                    $('#trans_{{$l->locale}}').val('');
                    @endforeach


        });

        $('.deltrans').click(function (e) {
            e.preventDefault();
            $('.deltransk').val($(this).data('tkey'));

        });
        
            
                
        $("#euemail").keyup(function(){
             if ($(this).val().indexOf('@') >= 0 && $(this).val().indexOf('.') >= 0 ) {
                  var email = $("#euemail").val();
                 $("#euemail").css("border-color","#ccc");
                  var data = {

                    _token : '{{csrf_token()}}',
                      email: email 
                    
                    
                };
                $.post('{{route('cms.settings.check.user')}}', data)
                    .done(function (data) {
                        console.log(data);

                     if(data.go == false){
                         $(".addNewUser").hide();
                         $("#euemail").css("border-color","#dc4545");
                     }
                     if(data.go == true){
                         $(".addNewUser").css("display","inline-block");
                     }
                    });   
                 
             } else { 
               $(".addNewUser").hide();
                $("#euemail").css("border-color","#dc4545");
             }

        });    

    </script>







@endsection